import * as auth_data from '../resources/services/authentication.js'
import * as url_data from '../resources/services/generalUrls.js'
import * as middlewaresProcedures from '../resources/services/middlewaresProcedures.js'
import * as dataSource from './data/dataSource.js'
import * as config_data from '../resources/configurations/scenarios.js'
import * as mockData from './data/mockData.js'

//Configurations available: hub_market_test, stress, soak, load, ramp
let response;
let localJson;
const  config= config_data.scenario(parseInt(__ENV.TEST_TYPE),"widgets_test")
export const options = config;
var authenticationData = auth_data.qaDataAuth;
var apiUrls = url_data.qaEnv;
var environment = parseInt(__ENV.ENV);
if(environment != 1){
  switch(environment){
    case 2: //PREPRODUCCI�N
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
    case 3: //PRODUCCION
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
  }
}
// var indexServiceToBeExecute = __ENV.indexService;
// if(indexServiceToBeExecute=null){
//   indexServiceToBeExecute = 0;
// }

function contentFunctions(){
  // login
  response = middlewaresProcedures.login(`${url_data.iam_admin_url}${auth_data.admin_path}`,
  auth_data.admin_body,auth_data.urlencooded_header);

  const authHeaders = {headers:{Authorization: `Bearer ${auth_data.headers["Authorization"]}`,'Content-Type': 'application/json',},};
  //Introduce 0 when call this method for execute all endpoints
  //Introduce any number inside object range, for example "5" if do you want execute individual test per only one service
  const limit =dataSource.getIndexNumber(1);

  //Test
  let i = limit[1];
  do {
    const payload = dataSource.obj[i];
   localJson = mockData.delegateMockDataPerService(i);
   middlewaresProcedures.postGraphQL(url_data.api_url+"/widgets/graphql",payload ,authHeaders,localJson);
    i = i-1;
  } while (i >= limit[0]);
  }
export default function widgets() {contentFunctions()}
export function widgets_test() {  contentFunctions()}