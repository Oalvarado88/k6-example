import * as inputs from '../../resources/extra/utils.js';
//Queries
//NOTA: Las verificaciones van a variar por ambiente ya que dependen directamente del query inyectado  vs los datos del ambiente
const defaultQueryQA = `{
    projects{
        id
        name
        workspaces{
            id
            name
            order
        }
        users{
            connectionId
            id
        }
    }
    workspaces(options: {offset: 0, limit: 10}){
        id
        name
        widgets{
            name
            id
            type
        }
    }
}`;


//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:defaultQueryQA,
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}