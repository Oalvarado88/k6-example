
let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case 1:
        mockObj={
            "data": {
                "projects": [
                    {
                        "id": "534b1ced-1871-4892-9fb3-6ec29945d17c",
                        "name": "QA",
                        "workspaces": [
                            {
                                "id": "0c5ae5c1-6057-4eb1-bdc7-db0593f14256",
                                "name": "Espacio de prueba o1",
                                "order": 1
                            },
                            {
                                "id": "db0ae1b5-e2a1-46ab-893e-fca18b04319a",
                                "name": "Nuevo WS",
                                "order": 3
                            },
                            {
                                "id": "0e875560-71a7-4567-b213-d53f6c01e834",
                                "name": "Espacio de prueba o1",
                                "order": 2
                            }
                        ],
                        "users": [
                            {
                                "connectionId": "a4204a8b-3743-473e-bbd1-caa279275f9a",
                                "id": "d7ea3fdc-904b-406d-a1ed-a05ddd5a9029"
                            },
                            {
                                "connectionId": "8be13cac-68bb-46af-8248-39e19df4178a",
                                "id": "b4e9d859-86f9-4960-b46b-b58d09589cc8"
                            }
                        ]
                    },
                    {
                        "id": "68ab4aa8-15a8-4ea4-bc12-037f0be511b5",
                        "name": "DEV",
                        "workspaces": [
                            {
                                "id": "1902c119-93dd-4925-a85b-c3368b666694",
                                "name": "Espacio Inicial 2",
                                "order": 2
                            },
                            {
                                "id": "b7038193-adec-491f-907e-946e26c224cc",
                                "name": "Espacio Inicial",
                                "order": 1
                            }
                        ],
                        "users": [
                            {
                                "connectionId": "866d0953-f20c-43b5-973d-19750490e307",
                                "id": "af7ff593-646b-430b-b93e-9ade4f67b863"
                            },
                            {
                                "connectionId": "8be13cac-68bb-46af-8248-39e19df4178a",
                                "id": "0a7c0e42-7b70-4f69-962e-ee7689ee3ef4"
                            }
                        ]
                    }
                ],
                "workspaces": [
                    {
                        "id": "c971e918-b6fb-4511-8b28-838801f7c383",
                        "name": "Espacio Inicial",
                        "widgets": []
                    },
                    {
                        "id": "1902c119-93dd-4925-a85b-c3368b666694",
                        "name": "Espacio Inicial 2",
                        "widgets": []
                    },
                    {
                        "id": "db0ae1b5-e2a1-46ab-893e-fca18b04319a",
                        "name": "Nuevo WS",
                        "widgets": []
                    },
                    {
                        "id": "0e875560-71a7-4567-b213-d53f6c01e834",
                        "name": "Espacio de prueba o1",
                        "widgets": [
                            {
                                "name": "Prueba widget no relacionado a Workspace de QA",
                                "id": "dbbf4ac6-f21e-4bd5-a6da-fcd48f9d010d",
                                "type": "TICKER_TAPE"
                            },
                            {
                                "name": "Prueba widget no relacionado a QAuser",
                                "id": "9198bae6-2c85-409c-8db0-a7cf6f16e8b2",
                                "type": "TICKER_TAPE"
                            }
                        ]
                    },
                    {
                        "id": "b7038193-adec-491f-907e-946e26c224cc",
                        "name": "Espacio Inicial",
                        "widgets": [
                            {
                                "name": "Prueba DTC Nuevo",
                                "id": "20c3e3ef-d010-40d9-a5b6-7e0722687b8d",
                                "type": "DYNAMIC_TABLE"
                            },
                            {
                                "name": "Prueba DTC CUATRO",
                                "id": "9d8ccd42-841a-4aac-8273-27a32327d32f",
                                "type": "DYNAMIC_TABLE"
                            },
                            {
                                "name": "BMV IPC",
                                "id": "ee7dc1f6-3f51-4acf-9411-b364c03edc60",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "CHART EXAMPLE",
                                "id": "37933f40-1177-4a8c-bf4c-1a945fae7f15",
                                "type": "CHART"
                            }
                        ]
                    },
                    {
                        "id": "0c5ae5c1-6057-4eb1-bdc7-db0593f14256",
                        "name": "Espacio de prueba o1",
                        "widgets": [
                            {
                                "name": "MULTIPLE UNIQUEKEYS",
                                "id": "1b42de71-872c-405e-a171-6d694d79a709",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "NEWS",
                                "id": "1947bcc2-72e3-4c65-b691-b3aa9a9eac09",
                                "type": "NEWS_LIST"
                            },
                            {
                                "name": "Custom table test 1",
                                "id": "64823e14-3dfe-4f51-bb7c-5613e071fedd",
                                "type": "CUSTOM_TABLE"
                            },
                            {
                                "name": "MULTIPLE",
                                "id": "4c2e62d8-52ac-4512-bd91-1a04c61049e6",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "Custom table test 1",
                                "id": "62996274-c1a8-45fc-b111-cc88ff04e452",
                                "type": "CUSTOM_TABLE"
                            },
                            {
                                "name": "MULTIPLE",
                                "id": "2a2188ce-e4d3-4a41-b791-c1dbc7c57e70",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "DOUBLE SAME DATA",
                                "id": "239d3664-862b-49e4-91a6-cea7fb0486c8",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "Prueba 1 tiker",
                                "id": "934f7736-fec2-4bd7-bf37-603f950672fb",
                                "type": "TICKER_TAPE"
                            },
                            {
                                "name": "Prueba 1 tiker",
                                "id": "d545bcd8-6ee6-4d4c-ab23-8aeb3e28dc34",
                                "type": "TICKER_TAPE"
                            },
                            {
                                "name": "Prueba 1 tiker",
                                "id": "bd58b16f-92ee-4c57-919a-1274ba9b79c2",
                                "type": "TICKER_TAPE"
                            },
                            {
                                "name": "Custom table test 1",
                                "id": "ce1f6dc9-b3a3-40fc-b4af-c0fe520a8a68",
                                "type": "CUSTOM_TABLE"
                            },
                            {
                                "name": "Prueba QA 2",
                                "id": "f63f3b70-3414-4e94-8b27-9eab15703854",
                                "type": "PREDEFINED_TABLE"
                            },
                            {
                                "name": "Prueba no widgets",
                                "id": "82af4245-f265-4a06-914c-d6197d9b40d8",
                                "type": "TICKER_TAPE"
                            }
                        ]
                    }
                ]
            }
        };        
        return mockObj;
        break;
     }
}

module.exports = {delegateMockDataPerService};

