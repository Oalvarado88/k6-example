import * as auth_data from '../resources/services/authentication.js'
import * as url_data from '../resources/services/generalUrls.js'
import * as procedures from '../resources/services/middlewaresProcedures.js'
import * as config_data from '../resources/configurations/scenarios.js'
import * as data from './data/index.js'

//Configurations available: 1-SmokeTest, 2-stress, 3-soak, 4-load, 5-ramp
const  config= config_data.scenario(parseInt(__ENV.TEST_TYPE),"workspace_api_Test")
export const options = config;

function contentFunctions() {
  // login
  procedures.login(`${url_data.hub_workspace_url}${auth_data.hub_path}`,
  JSON.stringify(auth_data.hub_body),
  {headers: auth_data.headers});

  const authHeaders = {headers:{Authorization: `Bearer ${auth_data.headers["Authorization"]}`},};
  //Introduce 0 when call this method for execute all endpoints
  //Introduce any number inside object range, for example "5" if do you want execute individual test per service
  
  const limit =data.getIndexNumber(1);
    //Test
    let i = limit[1];
  do {
    const st = data.obj[i];
  const object = st.split('#')
    procedures.get(url_data.hub_workspace_url,object[0],object[1],authHeaders);
    i = i-1;
  } while (i >= limit[0]);
}
export default function workspace_api() {contentFunctions()}
export function workspace_api_Test() {  contentFunctions()}