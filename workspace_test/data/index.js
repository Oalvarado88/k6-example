import * as inputs from '../../resources/extra/utils.js';

//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/api/v1/predefined_tables#`,
    2:`/api/v1/users/workspaces/2904#`,
    3:`/api/v1/users/workspaces#`,
    4:`/api/v1/markets#`,
    5:`/api/v1/instruments#`,
    6:`/api/v1/auth/users#`,
    7:`/api/v1/auth/users/permissions#`,
    8:`/api/v1/auth/users/508#`,
    9:`/api/v1/users/workspaces/14131#`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}
module.exports={obj,getIndexNumber}
