import * as inputs from '../../resources/extra/utils.js';

//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/am-xignite-adapter/api/v1/intraday#?period=5&interval=1&timing=realtime&limit&instrumentKey=${inputs.getRandomStockVal()}`, //intraday stock real time
    2:`/am-xignite-adapter/api/v1/intraday#?instrumentKey=${inputs.getRandomEUUInstrumentKey()}&period=5&interval=1&timing=realtime`, //Intraday stock delayed
    3:`/am-xignite-adapter/api/v1/intraday#?instrumentKey=${inputs.getRandomDIVISASInstrumentKey()}&period=1&interval=1&timing=realtime`,//Intraday currency Forex
    //4:`/am-xignite-adapter/api/v1/intraday#?instrumentKey=${inputs.getRandomFuturesInstrumetnKey()}&period=1&interval=1&timing=realtime`, //Intraday Futures Real time
    4:`/am-xignite-adapter/api/v1/symbols/symbol#?instrumentKey=${inputs.getRandomEUUInstrumentKey()}`, //Symbols findOne
    5:`/am-xignite-adapter/api/v1/historical#?instrumentKey=${inputs.getRandomEUUInstrumentKey()}&period=1&interval=D&order=desc`, //historical Stocks
    6:`/am-xignite-adapter/api/v1/historical#?instrumentKey=${inputs.getRandomIndexInstrumentKey()}&period=1&interval=D`, //Historical Indices
    7:`/am-xignite-adapter/api/v1/historical#?instrumentKey=${inputs.getRandomDIVISASInstrumentKey()}&period=1&interval=D`,//Historical Forex
    //9:`/am-xignite-adapter/api/v1/historical#?instrumentKey=${inputs.getRandomFuturesInstrumetnKey()}period=1&interval=D&order=desc` //Historical futures
    //3:`/api/v3/instruments/last#?instrumentKey=${inputs.getRandomBMVInstrumentKey()}`,
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}
module.exports={obj,getIndexNumber}
