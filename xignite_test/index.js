import * as auth_data from '../resources/services/authentication.js'
import * as url_data from '../resources/services/generalUrls.js'
import * as middlewaresProcedures from '../resources/services/middlewaresProcedures.js'
import * as config_data from '../resources/configurations/scenarios.js'
import * as dataSource from './data/dataSource.js'
//Configurations available: hub_market_test, stress, soak, load, ramp
const  config= config_data.scenario(parseInt(__ENV.TEST_TYPE),"xignite_api_test")
export const options = config;
var authenticationData = auth_data.qaDataAuth;
var apiUrls = url_data.qaEnv;
var environment = parseInt(__ENV.ENV);
if(environment != 1){
  switch(environment){
    case 2: //PREPRODUCCIÓN
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
    case 3: //PRODUCCION
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
  }
}
function contentFunctions() { 
  // login
  middlewaresProcedures.login(`${url_data.iam_admin_url}${auth_data.admin_path}`,
  auth_data.admin_body,auth_data.urlencooded_header);
  const authHeaders = {    headers: {Authorization: `Bearer ${auth_data.headers["Authorization"]}`},
  };
  //Introduce 0 when call this method for execute all endpoints
  //Introduce any number inside object range, for example "5" if do you want execute individual test per service
  const limit =dataSource.getIndexNumber(0);

    //Test
    let i = limit[1];
  do {
    const st = dataSource.obj[i];
  const object = st.split('#')
  middlewaresProcedures.get(apiUrls.api_url,object[0],object[1],authHeaders);
    i = i-1;
  } while (i >= limit[0]);
}
export default function xignite_api() {contentFunctions()}
export function xignite_api_test() {  contentFunctions()}