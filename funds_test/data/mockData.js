let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/funds/api/v1/administrators':
        mockObj={
            "administrators": [
                {
                    "administratorId": 2,
                    "description": "ACTINVER",
                    "businessName": "Operadora Actinver S.A. de C.V. Sociedad Operadora de Sociedades de Inversión, Grupo Financiero Actinver",
                    "webAddress": "http://www.actinver.com.mx/"
                },
                {
                    "administratorId": 4,
                    "description": "BBVA-BANCOMER",
                    "businessName": "BBVA Asset Management México, S.A. de C.V., Sociedad Operadora de Fondos de Inversión, Grupo Financiero BBVA México",
                    "webAddress": "https://www.bbva.mx/personas/productos/inversion/fondos-de-inversion.html"
                }
            ],
            "totalRows": 30
        };        
        return mockObj;
        break;
    case '/funds/api/v1/categories':
        mockObj={
            "categories": [
                {
                    "categoryId": "7fc6fdc6-0733-4561-bae2-39b5059bb206",
                    "fundTypeId": 100,
                    "fundType": "Renta Fija"
                },
                {
                    "categoryId": "8abdee87-ae0b-4f39-9cee-3ee0ab1aca11",
                    "fundTypeId": 101,
                    "fundType": "Renta Variable"
                }
            ],
            "totalRows": 2
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds':
        mockObj={
            "funds": [
                {
                    "id": 21076,
                    "marketTypeId": 1,
                    "valueTypeId": 13617,
                    "exchangeId": 0,
                    "symbol": "ACTICOBA",
                    "issuer": "ACTICOB",
                    "series": "A",
                    "isTrading": true,
                    "name": "Acticober, S.A. de C.V. Fondo de Inversión en Instrumentos de Deuda - - en adelante ACTICOB --",
                    "instrumentKey": "1/13617/0/ACTICOBA",
                    "isin": "MX52AC1A0080",
                    "active": true,
                    "exchangeValueType": "51",
                    "displaySymbol": "ACTICOBA",
                    "index": "",
                    "keywords": "FONDO SOCIEDAD INVERSION RENTA FIJA",
                    "createdAt": "2021-06-14 12:44:40",
                    "assetClass": null,
                    "exchange": null,
                    "sectors": []
                },
                {
                    "id": 39136,
                    "marketTypeId": 1,
                    "valueTypeId": 13617,
                    "exchangeId": 0,
                    "symbol": "ACTICOBB",
                    "issuer": "ACTICOB",
                    "series": "B",
                    "isTrading": true,
                    "name": "Acticober, S.A. de C.V. Fondo de Inversión en Instrumentos de Deuda - - en adelante ACTICOB --",
                    "instrumentKey": "1/13617/0/ACTICOBB",
                    "isin": "MX51AC0F00J5",
                    "active": true,
                    "exchangeValueType": "51",
                    "displaySymbol": "ACTICOBB",
                    "index": "",
                    "keywords": "",
                    "createdAt": "2021-11-09 19:00:29",
                    "assetClass": null,
                    "exchange": null,
                    "sectors": []
                }
            ],
            "totalRows": 125
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/portfolio/detail':
        mockObj={
            "portfolioDetail": [
                {
                    "fundId": 772,
                    "issuer": "DEUDAOPB1",
                    "underlyingId": 79,
                    "underlyingAsset": "CETES",
                    "series": "241003",
                    "underlyingHoldingAmount": 1909138163,
                    "underlyingHoldingPercentage": 12.743874624757808
                },
                {
                    "fundId": 772,
                    "issuer": "DEUDAOPB1",
                    "underlyingId": 79,
                    "underlyingAsset": "CETES",
                    "series": "241128",
                    "underlyingHoldingAmount": 1103743415,
                    "underlyingHoldingPercentage": 7.3677054763595065
                }
            ],
            "totalRows": 161
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/prospectus':
        mockObj={
            "fundId": "100000",
            "issuer": "APIBONO",
            "bussinesName": "FONDO BANORTE 33, S.A. DE C.V. F.I.I.D.",
            "fundDateAuth": "2022-08-12T05:00:00.000Z",
            "fundAssetClass": "LARGO PLAZO GUBERNAMENTAL (IDLPGUB)",
            "fundTerm": "largo plazo",
            "investmentRegime": "",
            "liquidity": "Diaria (Todos los días hábiles).",
            "lockInPeriod": "No tiene.",
            "netAsset": 0,
            "assetsUnderManagement": 0,
            "fundHoldings": 0,
            "marketRating": "AAA/6",
            "creditRating": "",
            "fundManager": "Don Alejandro Aguilar Ceballos               ",
            "active": true,
            "closeTime": "14:30",
            "fundType": "Renta Fija",
            "administrator": "Operadora de Fondos Banorte, S.A. de C.V., Sociedad Operadora de Fondos de Inversión Grupo Financiero Banorte.",
            "series": [
                "A",
                "APIE",
                "APIF",
                "APIM",
                "E1",
                "E2",
                "E3",
                "E4",
                "E5",
                "F1",
                "F2",
                "F3",
                "F4",
                "FF",
                "M1",
                "M2",
                "M3",
                "M4",
                "M5"
            ]
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/yields':
        mockObj={
            "dailyYield": -0.004016172396015261,
            "weeklyYield": 0.1584394129239275,
            "monthlyYield": 0.45964563499414357,
            "annualYield": 2.4375431293234495,
            "yearToDateYield": 0.71070343644557,
            "dailyYieldAnnualized": -1.4458220625654938,
            "weeklyYieldAnnualized": 8.482495629757935,
            "monthlyYieldAnnualized": 5.657347237830046,
            "yearToDateYieldAnnualized": 4.415879833718317
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/last-price':
        mockObj={
            "price": 2.463724
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/share-Classes':
        mockObj={
            "series": "APIM",
            "minimumEntryLoad": "Sin monto mínimo de inversión",
            "advisoryFee": "n/a",
            "managementFee": "0.035",
            "shareHolders": "PERSONAS MORALES (artículo 106 fracción IV, inciso b) de CUFI)",
            "active": true
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/overview':
        mockObj={
            "funds": [
                {
                    "symbol": "APIBONOM5",
                    "bussinesName": "FONDO BANORTE 33, S.A. DE C.V. F.I.I.D.",
                    "fundType": "Renta Fija",
                    "fundAssetClass": "LARGO PLAZO GUBERNAMENTAL (IDLPGUB)",
                    "isin": "MX51AP1R00A2",
                    "administrator": "BANORTE CB",
                    "shareHolders": "PERSONAS MORALES",
                    "close": 2.197906,
                    "lastPrice": 2.20242,
                    "changePercentage": 0.2053773,
                    "advisoryFee": "n/a",
                    "managementFee": "0.735",
                    "closeTime": "14:30",
                    "dateLastPrice": "2023-10-10"
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/portfolio':
        mockObj={
            "portfolio": [
                {
                    "underlyingId": 115,
                    "underlyingAssetClass": "BONDESD - BONOS DES GF BONDESD",
                    "underlyingHoldingPercentage": 14.537354767144302,
                    "underlyingHoldingAmount": 2177816370,
                    "rebalanceDate": "2023-02-28T06:00:00.000Z"
                },
                {
                    "underlyingId": 90,
                    "underlyingAssetClass": "BONO - BONOS GOB FED TASA FIJA",
                    "underlyingHoldingPercentage": 3.197930905798528,
                    "underlyingHoldingAmount": 479076585,
                    "rebalanceDate": "2023-02-28T06:00:00.000Z"
                },
                {
                    "underlyingId": 140,
                    "underlyingAssetClass": "BPA - BPA PAGO MENSUAL INTERES",
                    "underlyingHoldingPercentage": 2.176395133744449,
                    "underlyingHoldingAmount": 326042050,
                    "rebalanceDate": "2023-02-28T06:00:00.000Z"
                }
            ],
            "totalRows": 21
        };        
        return mockObj;
        break;
    case '/funds/api/v1/funds/historical-prices':
        mockObj={
            "historicalPrices": [
                {
                    "date": "2023-09-29T05:00:00.000Z",
                    "high": 2.453142,
                    "low": 2.453142,
                    "close": 2.453142
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
     }
}


module.exports = {delegateMockDataPerService};