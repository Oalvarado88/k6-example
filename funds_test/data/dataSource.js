import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/funds/api/v1/administrators#`,
    2:`/funds/api/v1/categories#`,
    3:`/funds/api/v1/funds#?fundTypeId=100&administratorId=2`,
    4:`/funds/api/v1/funds/portfolio/detail#?underlyingId=0&issuer=DEUDAOP&series=B1`,
    5:`/funds/api/v1/funds/prospectus#?issuer=APIBONO&series=APIE`,
    6:`/funds/api/v1/funds#?administratorid=126`,
    7:`/funds/api/v1/funds/yields#?issuer=${inputs.getRanndomIssuer()}&series=${inputs.getRandomSerie()}&date=2022-03-01`,
    8:`/funds/api/v1/funds/last-price#?issuer=${inputs.getRanndomIssuer()}&series=${inputs.getRandomSerie()}`,
    9:`/funds/api/v1/funds/share-Classes#?issuer=MIFDLLS&series=BF`,
    10:`/funds/api/v1/funds/overview#?issuer=APIBONO&series=M5`,
    11:`/funds/api/v1/funds/portfolio#?issuer=${inputs.getRanndomIssuer()}&series=${inputs.getRandomSerie()}`,
    12:`/funds/api/v1/funds/historical-prices#?issuer=${inputs.getRanndomIssuer()}&series=${inputs.getRandomSerie()}&&period=1&&interval=M`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}