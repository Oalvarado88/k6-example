import * as auth_data from '../resources/services/authentication.js'
import * as url_data from '../resources/services/generalUrls.js'
import * as middlewaresProcedures from '../resources/services/middlewaresProcedures.js'
import * as config_data from '../resources/configurations/scenarios.js'
import * as dataSource from './data/dataSource.js'
import * as mockData from './data/mockData.js'

//Configurations available: hub_market_test, stress, soak, load, ramp
const  config= config_data.scenario(parseInt(__ENV.TEST_TYPE),"hub_market_api_Test")
let localJson;
export const options = config;
var authenticationData = auth_data.qaDataAuth;
var apiUrls = url_data.qaEnv;
var environment = parseInt(__ENV.ENV);
if(environment != 1){
  switch(environment){
    case 2: //PREPRODUCCI�N
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
    case 3: //PRODUCCION
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
  }
}
// var indexServiceToBeExecute = __ENV.indexService;
// if(indexServiceToBeExecute=null){
//   indexServiceToBeExecute = 0;
// }

function contentFunctions() {  
  // login
  middlewaresProcedures.login(`${apiUrls.hub_workspace_url}${auth_data.hub_path}`,
  JSON.stringify(authenticationData.hub_body),
  {headers: auth_data.headers});
  
  const authHeaders = {headers:{Authorization: `Bearer ${auth_data.headers["Authorization"]}`},};
  //Introduce 0 when call this method for execute all endpoints
  //Introduce any number inside object range, for example "5" if you want execute individual test per service
  const limit =dataSource.getIndexNumber(0);

    //Test
    let i = limit[1];
  do {
    const st = dataSource.obj[i];
  const object = st.split('#')
  console.log(apiUrls.hub_market_url,object[0],object[1])
  localJson = mockData.delegateMockDataPerService(object[0]);
  middlewaresProcedures.getWithMockValidation(apiUrls.hub_market_url,object[0],object[1],authHeaders,localJson);
    i = i-1;
  } while (i >= limit[0]);
}
export default function hub_market_api() {contentFunctions()}
export function hub_market_api_Test() {  contentFunctions()}