let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/api/v1/instruments/stocks/mappings':
        mockObj={
            "data": [
                {
                    "instrument_id": 185,
                    "instrumentsMapping_id": null,
                    "marketTypeId": 1,
                    "valueTypeId": 12609,
                    "exchangeId": 0,
                    "symbol": "036570N",
                    "active": 0,
                    "createdAt": "2022-02-22T16:14:20.000Z",
                    "updatedAt": "2022-02-22T16:14:20.000Z",
                    "provider_symbol_code": null
                }
            ]
        };        
        return mockObj;
        break;
    case '/api/v1/instruments/historical/financial/ratios':
            mockObj={
                "data": [
                    {
                        "RFId": 13,
                        "description": "Inc Per Employee Tot Ops"
                    }
                ],
                "metadata": {
                    "statusbar": 200
                }
            };        
        return mockObj;
            break;
    case '/api/v3/instruments/report/market/capitalization':
        mockObj={
            "data": {
                "capitalizationDetail": [
                    {
                        "issuer": "AC",
                        "serie": "*",
                        "outstandingShares": 1740491160,
                        "outstandingSharesDate": "2023-09-27T00:00:00.000Z",
                        "marketCapitalization": 272821989330,
                        "marketCapitalizationDate": "2023-09-27T00:00:00.000Z"
                    }
                ],
                "totalCapitalization": {
                    "outstandingShares": 1740491160,
                    "marketCapitalization": 272821989330,
                    "outstandingSharesDate": "2023-09-27T00:00:00.000Z",
                    "marketCapitalizationDate": "2023-09-27T00:00:00.000Z"
                }
            },
            "metadata": {
                "statuscode": 200
            }
        };        
        return mockObj;
        break;
    case '/api/v1/healthcheck':
        mockObj={
            "data": {
                "message": "OK",
                "timestamp": 1695923754609
            },
            "metadata": {
                "statuscode": 200
            }
        };        
        
        return mockObj;
        break;
    case '/api/v3/instruments/stock/screener':
        mockObj={
            "data": [
                {
                    "id": 1076,
                    "marketTypeId": 1,
                    "valueTypeId": 12617,
                    "exchangeId": 0,
                    "symbol": "DZK*",
                    "issuer": "DZK",
                    "series": "*",
                    "isTrading": 0,
                    "name": "DIREXION DAILY MSCI DEVELOPED MARKETS BULL 3X SHARES",
                    "instrumentKey": "1/12617/0/DZK*",
                    "isin": "US25459W7891",
                    "active": 1,
                    "infoselKey": "DZK*.",
                    "exchangeValueType": "1I",
                    "group": null,
                    "marketId": "1",
                    "deactivateDt": "2022-03-10 12:03:26",
                    "lastPriceDt": null,
                    "createdAt": "2021-06-14 12:40:11",
                    "updatedAt": "2023-07-20 17:31:31",
                    "descriptionES": null,
                    "descriptionEN": null
                }                
            ],
            "metadata": {
                "statuscode": 200
            }
        };        
        
        return mockObj;
        break;
    case '/api/v3/instruments/columns':
        mockObj={
            "data": [
                {
                    "id": 822,
                    "uniqueKey": "1/13088/0",
                    "name": "instrumento",
                    "description": "Instrumento",
                    "format": "{\"format\": \"text\" }"
                }
            ],
            "metadata": {
                "statuscode": 200,
                "totalColumns": 63
            }
        };        
        
        return mockObj;
        break;       
        case '/api/v1/instruments/historical/financial/ratios':
            mockObj={
                "data": [
                    {
                        "2019": 1.1940480470657349,
                        "2020": 1.720810055732727,
                        "2021": 1.976842999458313,
                        "2022": 2.369533061981201,
                        "RFId": 151,
                        "description": "Total Debt To Equity"
                    }    ],
                    "metadata": {
                        "statusbar": 200
                    }
                };        
            return mockObj;
            break;
        case '/api/v1/instruments/stocks/recommendation-trends':
                mockObj={
                    "data": {
                        "instrumentKey": "1/12576/0/Q*",
                        "ticker_region": "Q-MX",
                        "consensusStartDate": "2023-07-14T00:00:00+00:00",
                        "buy": 1,
                        "hold": 3,
                        "sell": 1,
                        "over": 0,
                        "under": 1
                    }
                };        
            return mockObj;
                break;
        case '/api/v1/instruments/report/market/capitalization':
            mockObj={
                "data": {
                    "capitalizationDetail": [
                        {
                            "issuer": "AAPL",
                            "serie": "*",
                            "outstandingShares": 15647868000,
                            "outstandingSharesDate": "2023-06-30T06:00:00.000Z",
                            "marketCapitalization": 2664542.117152,
                            "marketCapitalizationDate": "2023-09-27T00:00:00+00:00",
                            "currency": "USD"
                        }
                    ],
                    "totalCapitalization": {
                        "outstandingShares": 15647868000,
                        "marketCapitalization": 2664542.117152,
                        "outstandingSharesDate": "2023-06-30T06:00:00.000Z",
                        "marketCapitalizationDate": "2023-09-27T00:00:00+00:00"
                    }
                },
                "metadata": {
                    "statuscode": 200
                }
            };        
            
            return mockObj;
            break;
        case '/api/v3/instruments/detail/participation':
            mockObj={
                "data": [
                    {
                        "casaBolsa": "ACTIN",
                        "clave": "C",
                        "numeroOperacion": 114,
                        "volumen": 28113,
                        "importe": 4425284.799999999,
                        "precioPromedio": 157.411,
                        "porcentajeParticipacion": 2.097
                    }],
                    "totals": [
                        {
                            "rowNumber": "1",
                            "clave": "X",
                            "numeroOperacion": 739,
                            "volumen": 122282,
                            "importe": 19259436.491200007,
                            "porcentajeParticipacion": 9.125
                        }
                    ],
                    "metadata": {
                        "statuscode": 200
                    }
                };        
            
            return mockObj;
            break;
        case '/api/v3/instruments/detail/intraday':
            mockObj={
                "data": [
                    {
                        "rowNumber": 1,
                        "foliobmv": 8881,
                        "fechahecho": 22916,
                        "fechahora": "2023-09-28T14:00:05.000Z",
                        "fecha": "2023-09-28",
                        "hora": "14:00",
                        "precio": 157.4914,
                        "cantidad": 588,
                        "importe": 92604.9432,
                        "comprador": "CITI",
                        "vendedor": "CITI",
                        "preciopp": 158.070007,
                        "Liquidacion": "4  ",
                        "HoraCompleta": "14:00:05",
                        "PicoLote": "P"
                    }
                ],
                "Emisora": "AC*",
                "metadata": {
                    "total": 8881,
                    "limit": 1,
                    "offset": 1,
                    "statuscode": 200
                }
            };        
            
            return mockObj;
            break;
        case '/api/v3/instruments/historical/chart':
            mockObj={
                "data": [
                    {
                        "datetime": "2023-08-29T00:00:00.000Z",
                        "open": 163.82,
                        "close": 164.07,
                        "high": 165.48,
                        "low": 162.04,
                        "volume": "1305166"
                    } ],
                    "metadata": {
                        "statuscode": 200
                    }
                };        
            
            return mockObj;
            break;
            case '/api/v3/instruments/intraday/chart':
                mockObj={
                    "data": [{
                    "datetime": "2023-09-28T13:59:00.000Z",
                    "open": 158.01001,
                    "close": 158.070007,
                    "high": 158.050003,
                    "low": 158,
                    "volume": "1182"
                }
            ],
            "metadata": {
                "statuscode": 200
            }
        };        
                
                return mockObj;
                break;
        case '/api/v3/instruments/detail/intraday/report':
            mockObj = {
                "data": [
                    {
                        "rowNumber": 1,
                        "Fecha": "2023-09-11T14:00:05.000Z",
                        "Comprador": "FMX  ",
                        "cveComprador": 134,
                        "Vendedor": "FMX  ",
                        "cveVendedor": 134,
                        "Volumen": 28,
                        "Precio": 159.63,
                        "Importe": 4469.639999999999,
                        "PicoLote": "P"
                    }
                ],
                "metadata": {
                    "totalRows": 6472,
                    "page": 1,
                    "rows": 1,
                    "startRow": 1,
                    "finalRow": 1,
                    "statuscode": 200
                }
            }
            return mockObj;
            break;
        case '/api/v3/instruments/detail/depth':
            mockObj = {
                "data": [
                    {
                        "Fecha": "2023-09-29T13:25:15.000Z",
                        "Simbolo": "AC*",
                        "Orderbook": 3367,
                        "PrecioCompra1": 158.14,
                        "NumOpCompra1": 3,
                        "VolAcCompra1": 201,
                        "PrecioCompra2": 158.11,
                        "NumOpCompra2": 1,
                        "VolAcCompra2": 121,
                        "PrecioCompra3": 158.07,
                        "NumOpCompra3": 1,
                        "VolAcCompra3": 332,
                        "PrecioCompra4": 157.51,
                        "NumOpCompra4": 1,
                        "VolAcCompra4": 1100,
                        "PrecioCompra5": 157.5,
                        "NumOpCompra5": 1,
                        "VolAcCompra5": 6,
                        "PrecioCompra6": 157.39,
                        "NumOpCompra6": 1,
                        "VolAcCompra6": 100,
                        "PrecioCompra7": 157.01,
                        "NumOpCompra7": 1,
                        "VolAcCompra7": 11,
                        "PrecioCompra8": 157,
                        "NumOpCompra8": 1,
                        "VolAcCompra8": 300,
                        "PrecioCompra9": 156.8,
                        "NumOpCompra9": 7,
                        "VolAcCompra9": 1215,
                        "PrecioCompra10": 156.38,
                        "NumOpCompra10": 1,
                        "VolAcCompra10": 5000,
                        "PrecioCompra11": 155.8,
                        "NumOpCompra11": 1,
                        "VolAcCompra11": 317,
                        "PrecioCompra12": 155.5,
                        "NumOpCompra12": 1,
                        "VolAcCompra12": 500,
                        "PrecioCompra13": 155.08,
                        "NumOpCompra13": 1,
                        "VolAcCompra13": 13,
                        "PrecioCompra14": 155,
                        "NumOpCompra14": 1,
                        "VolAcCompra14": 100,
                        "PrecioCompra15": 154.9,
                        "NumOpCompra15": 1,
                        "VolAcCompra15": 100,
                        "PrecioCompra16": 153,
                        "NumOpCompra16": 1,
                        "VolAcCompra16": 1,
                        "PrecioCompra17": 152.28,
                        "NumOpCompra17": 1,
                        "VolAcCompra17": 75,
                        "PrecioCompra18": 150.08,
                        "NumOpCompra18": 1,
                        "VolAcCompra18": 100,
                        "PrecioCompra19": 150,
                        "NumOpCompra19": 1,
                        "VolAcCompra19": 1,
                        "PrecioCompra20": 0,
                        "NumOpCompra20": 0,
                        "VolAcCompra20": 0,
                        "PrecioVenta1": 158.16,
                        "NumOpVenta1": 7,
                        "VolAcVenta1": 550,
                        "PrecioVenta2": 158.2,
                        "NumOpVenta2": 1,
                        "VolAcVenta2": 100,
                        "PrecioVenta3": 158.24,
                        "NumOpVenta3": 3,
                        "VolAcVenta3": 400,
                        "PrecioVenta4": 158.25,
                        "NumOpVenta4": 1,
                        "VolAcVenta4": 600,
                        "PrecioVenta5": 158.26,
                        "NumOpVenta5": 1,
                        "VolAcVenta5": 600,
                        "PrecioVenta6": 158.27,
                        "NumOpVenta6": 1,
                        "VolAcVenta6": 10,
                        "PrecioVenta7": 158.3,
                        "NumOpVenta7": 1,
                        "VolAcVenta7": 98,
                        "PrecioVenta8": 158.33,
                        "NumOpVenta8": 1,
                        "VolAcVenta8": 10,
                        "PrecioVenta9": 158.34,
                        "NumOpVenta9": 1,
                        "VolAcVenta9": 10,
                        "PrecioVenta10": 158.36,
                        "NumOpVenta10": 1,
                        "VolAcVenta10": 748,
                        "PrecioVenta11": 158.66,
                        "NumOpVenta11": 2,
                        "VolAcVenta11": 20,
                        "PrecioVenta12": 158.68,
                        "NumOpVenta12": 1,
                        "VolAcVenta12": 10,
                        "PrecioVenta13": 158.78,
                        "NumOpVenta13": 2,
                        "VolAcVenta13": 20,
                        "PrecioVenta14": 158.79,
                        "NumOpVenta14": 2,
                        "VolAcVenta14": 20,
                        "PrecioVenta15": 158.8,
                        "NumOpVenta15": 2,
                        "VolAcVenta15": 20,
                        "PrecioVenta16": 158.84,
                        "NumOpVenta16": 1,
                        "VolAcVenta16": 10,
                        "PrecioVenta17": 158.85,
                        "NumOpVenta17": 1,
                        "VolAcVenta17": 100,
                        "PrecioVenta18": 158.92,
                        "NumOpVenta18": 1,
                        "VolAcVenta18": 10,
                        "PrecioVenta19": 158.93,
                        "NumOpVenta19": 1,
                        "VolAcVenta19": 10,
                        "PrecioVenta20": 158.94,
                        "NumOpVenta20": 1,
                        "VolAcVenta20": 10
                    }
                ],
                "metadata": {
                    "statuscode": 200
                }
            };
            return mockObj;
            break;

        case '/api/v3/instruments/detail/xbrlReport':
                mockObj={
                    "data": [
                        {
                            "reportType": 19,
                            "exercises": "2021",
                            "catalogName": "Catálogo XBRL MXN",
                            "values": [{
                                "Llave": 7000,
                                "Clave": "7000",
                                "Partida": "Estado de situación financiera, circulante/no circulante",
                                "IV_2021_RCD": null,
                                "III_2021_RC": null,
                                "II_2021_RC": null,
                                "I_2021_RC": null
                            } ]
                        }
                    ],
                    "metadata": {
                        "status": 200
                    }
                };        
                return mockObj;
                break;
        case '/api/v3/instruments/historical/financialRatios/multiples':
                mockObj={
                    "data": [
                        {
                            "fecha": "2023-09-14T06:00:00.000Z",
                            "precioUtilidad": 16.677599153719672,
                            "precioValorLibros": 2.6198663553304136,
                            "precioVentas": 1.2749985059964264,
                            "evEbitda": 7.931833551648579
                        },
                        {
                            "fecha": "2023-09-15T06:00:00.000Z",
                            "precioUtilidad": 16.659785683902307,
                            "precioValorLibros": 2.617068056257744,
                            "precioVentas": 1.2736366704471718,
                            "evEbitda": 7.924693860611511
                        }
                    ],
                    "metadata": {
                        "statuscode": 200
                    }
                };        
                return mockObj;
                break;
        case '/api/v3/report/brokerageFirms/transactionsExtract':
                    mockObj={
                        "data": [{
                            "brokerageFirm": "ACTIN          ",
                            "transactionsNumber": 2564,
                            "volume": 2684672,
                            "amount": 66323808.50900003,
                            "sellPercentage": 3.5014512780737537,
                            "buyPercentage": 2.7910087886252155,
                            "totalPercentage": 3.149783561140175
                        }],
                        "totals": [
                            {
                                "transactionsNumber": 201751,
                                "volume": 85233539,
                                "amount": 3825036428.8664913,
                                "sellPercentage": 100,
                                "buyPercentage": 100.00000000000001,
                                "totalPercentage": 100
                            }
                        ],
                        "metadata": {
                            "statuscode": 200
                        }
                    };        
                return mockObj;
                    break;
        case '/api/v1/instruments/stocks/estimates':
                mockObj={
                    "data": [
                        {
                            "ticker_region": "AC-MX",
                            "item": "DPS",
                            "fiscalPeriodEndDate": "2023-12-31T00:00:00+00:00",
                            "consensusStartDate": "2023-09-13T00:00:00+00:00",
                            "consensusEndDate": null,
                            "relativePeriod": 1,
                            "currency": "MXN",
                            "mean": 6.912754,
                            "median": 7.181193,
                            "low": 3.5,
                            "high": 8.56,
                            "standardDeviation": 1.444715,
                            "numberOfEstimates": 10,
                            "numberOfEstimatesRevisedUpwards": 3,
                            "numberOfEstimatesRevisedDownwards": 0
                        }
                    ]
                };        
                
                return mockObj;
                break;
        case '/api/v3/instruments/last/dynamic':
                mockObj={
                    "data": [
                        {
                            "bolsa": 0,
                            "precioActual": 0.012,
                            "hora": "13:34:47",
                            "precioAnterior": 0.014,
                            "porcentaje": -14.2857,
                            "variacion": -0.002,
                            "volumenTotal": 415654,
                            "fechaPrecioActual": "2023-09-29T00:00:00.000Z",
                            "volumenHecho": 3249,
                            "oportunidadInformacion": "realTime",
                            "instrumentKey": "1/12576/0/HOMEX*",
                            "descripcion": "HOMEX*"
                        }
                    ],
                    "metadata": {
                        "statuscode": 200
                    }
                };        
                
                return mockObj;
                break;
        case '/api/v3/instruments/detail/financial/annual':
                mockObj={
                    "data": [{
                        "rubroID": 19176,
                        "TipoReporte": 19,
                        "descripcion": "Estado de Resultados",
                        "separador": true,
                        "Expresado": null,
                        "R_2022": null
                    }],
                "metadata": {
                    "statuscode": 200
                }
            };        
                
                return mockObj;
                break;
        case '/api/v3/instruments/report/sectorial/analysis/multiples':
                mockObj={
                    "data": [
                        {
                            "marketType": 1,
                            "valueType": 12576,
                            "exchange": 0,
                            "symbol": "AC*",
                            "issuer": "AC",
                            "serie": "*",
                            "isin": "MX01AC100006",
                            "priceToBookValue": 2.601924320099765,
                            "priceToEarnings": 16.56338337665537,
                            "priceToSales": 1.2662667368864986,
                            "lastUpdate": "2023-09-28T06:00:00.000Z",
                            "rowNumber": 1
                        }
                    ],
                    "metadata": {
                        "statuscode": 200,
                        "total": 108,
                        "limit": 2,
                        "offset": 0
                    }
                };        
                
                return mockObj;
                break;
        case '/api/v1/instruments/report/local/recommendations':
                    mockObj={
                        "data": [
                            {
                                "brokerageHouse": "Barclays",
                                "recommendation": "MANTENER",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "Actinver",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "Banorte",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "Monex",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "Invex",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "GBM",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            },
                            {
                                "brokerageHouse": "Intercam",
                                "recommendation": "COMPRA",
                                "date": "04/08/23"
                            }
                        ],
                        "metadata": {
                            "statuscode": 200
                        }
                    };        
                    return mockObj;
                    break;
        case '/api/v3/instruments/report/brokerageFirms':
                        mockObj={
                            "data": [
                                {
                                    "rowNumber": 1,
                                    "id": 33304,
                                    "brokerageFirm": "VE POR MÁS",
                                    "title": "(VE POR MÁS) PREVIO 3T - ARCA CONTINENTAL",
                                    "date": "2022-10-11",
                                    "time": "16:44",
                                    "symbol": "AC*",
                                    "urlFile": "https://contribucioncb.infosel.com/contribucion/archivosweb/documentos/2022/10/11/20221011333041.pdf"
                                }
                            ],
                            "metadata": {
                                "statuscode": 200
                            }
                        };        
                    return mockObj;
                        break;
        case '/api/v1/instruments/stocks/price-target':
                    mockObj={
                        "data": {
                            "instrumentKey": "1/12576/0/AC*",
                            "ticker_region": "AC-MX",
                            "consensusStartDate": "2023-09-27T00:00:00+00:00",
                            "baseCurrency": "MXN",
                            "mean": 193.4,
                            "median": 196.5,
                            "low": 145,
                            "high": 215,
                            "standardDeviation": 19.452506,
                            "numberOfEstimates": 16,
                            "numberOfEstimatesRevisedUpwards": 4,
                            "numberOfEstimatesRevisedDownwards": 2,
                            "targetCurrency": "MXN",
                            "meanTargetCurrency": 193.4,
                            "medianTargetCurrency": 196.5,
                            "lowTargetCurrency": 145,
                            "highTargetCurrency": 215
                        }
                    };        
                    
                    return mockObj;
                    break;
        case '/api/v3/instruments/last':
                    mockObj={
                        "data": [
                            {
                                "instrumento": "AC*",
                                "descripcionInstrumento": "ARCA CONTINENTAL, S.A.B. DE C.V.",
                                "emisora": "AC",
                                "serie": "*",
                                "precioActual": "158.77",
                                "importeTotal": "167581984.771",
                                "hora": "13:59:59",
                                "monitorHora": "13:30:00",
                                "monitorPrecio": 158.3,
                                "precioAnterior": "158.07",
                                "porcentaje": "0.442842",
                                "variacion": "0.70",
                                "volumenTotal": "1059465",
                                "posturaPrecioCompra": "158.61",
                                "posturaVolumenCompra": "600",
                                "posturaPrecioVenta": "158.77",
                                "posturaVolumenVenta": "15273",
                                "precioMaximoDia": "159.33",
                                "precioMinimoDia": "156.64",
                                "numeroOperaciones": "7424",
                                "operacion": "P",
                                "precioPromedio": 158.103,
                                "monitorVolumen": 161,
                                "monitorFecha": "29-09-2023",
                                "folioCid": 255739,
                                "isin": "MX01AC100006",
                                "fechaPrecioActual": "29-09-2023",
                                "posturaFecha": "29-09-2023",
                                "precioPP": "158.28",
                                "pppOficial": "",
                                "aireCierre": 0,
                                "precioApertura": "157.7",
                                "precioMaximo": "183.03",
                                "precioMinimo": "138.86",
                                "posturaHora": "13:59:00",
                                "fechaPrecioAnterior": "28-09-2023",
                                "volumenHecho": "100",
                                "comprador": "BTGP",
                                "vendedor": "FMX",
                                "accion": 2183,
                                "picoLote": "P",
                                "precioPico": 158.3,
                                "volumenPico": 48,
                                "importePico": 7598.4,
                                "fechaPico": "29-09-2023",
                                "rendimientoSemanal": -1.9267703364103745,
                                "rendimientoMensual": -4.333111742309777,
                                "rendimientoAnual": 10.328965709506567,
                                "rendimientoAlAnio": 0.037917087967645514,
                                "volumenMaximo52s": 67491050,
                                "volumenMinimo52s": 61866,
                                "volumenPromedio3a": 1559534.9076923076,
                                "volumenPromedio3m": 1766214.6200527705,
                                "horaPico": "13:30:07",
                                "capitalizaciónMercado": "344.44B",
                                "valorEmpresa": "541.78B",
                                "divYield": "5.20%",
                                "precioObjetivo": "50.37",
                                "recomendación": "Compra",
                                "tipoInstrumento": "ACCIÓN",
                                "spread": "0.16",
                                "spreadPorcentaje": "0.1009",
                                "oportunidadInformacion": "realTime",
                                "instrumentKey": "1/12576/0/AC*",
                                "descripcion": "AC*"
                            }
                        ],
                        "metadata": {
                            "statuscode": 200
                        }
                    };        
                    
                    return mockObj;
                    break;
        case '/api/v3/instruments/detail/profile':
                    mockObj={
                        "data": [
                            {
                                "Emisora": "AC",
                                "RazonSocial": "ARCA CONTINENTAL, S.A.B. DE C.V.",
                                "Descripcion": "Arca Continental es una empresa dedicada a la producción, distribución y venta de bebidas de las marcas propiedad de The Coca-Cola Company en México, Estados Unidos, Perú, Ecuador y Argentina, así como de botanas saladas bajo las marcas Bokados en México, Inalecsa en Ecuador, así como Wise y Deep River en los Estados Unidos. Arca Continental es el segundo embotellador de Coca-Cola más grande de América. Se formó en 2011 mediante la fusión de Embotelladoras Arca y Grupo Continental.",
                                "TipoEmpresa": "",
                                "Sector": "Productos de primera necesidad",
                                "SubSector": "ALIMENTOS, BEBIDAS Y TABACO",
                                "Ramo": "BEBIDAS",
                                "SubRamo": "PRODUCCIÓN DE BEBIDAS NO ALCOHÓLICAS",
                                "AccionesCirculacion": 1740491160,
                                "VolumenOperado": "121421",
                                "FechaFundacion": "1980-09-24T06:00:00.000Z",
                                "EBITDA": "$8024 millones",
                                "DatosContacto": "LIC. FELIPE BARQUIN  felipe.barquin@arcacontal.com",
                                "Fundadores": null,
                                "Filiales": null,
                                "Directivos": "ARTURO GUTIERREZ HERNANDEZ DIRECTOR GENERAL, MANUEL L. BARRAGAN MORALES PRESIDENTE DEL CONSEJO, EMILIO MARCOS CHARUR DIRECTOR DE ADMINISTRACION Y FINANZAS, FELIPE BARQUIN  RELACION CON INVERSIONISTAS,",
                                "Empleados": 0,
                                "Ubicacion": "MONTERREY, NL, MEXICO",
                                "Direccion": "Av. San Jerónimo 813, Pte. San Jerónimo, Monterrey, Nuevo León, 64640",
                                "SitioWeb": "http://www.arcacontal.com/",
                                "directorGeneral": "Arturo Gutiérrez Hernández",
                                "directorFinanciero": "Emilio Marcos Charur",
                                "email": "felipe.barquin@arcacontal.com"
                            }
                        ],
                        "metadata": {
                            "statuscode": 200
                        }
                    };        
                    
                    return mockObj;
                    break;
        case '/api/v3/instruments/historical/fund/returns':
                    mockObj={
                        "data": [
                            {
                                "date": "2023-09-05T00:00:00.000Z",
                                "price": 2.213696,
                                "yearToDate": 4.214340054684751,
                                "yearToDateCompounded": 6.201126770379961,
                                "twelveMonths": 6.49542954484994,
                                "twelveMonthsCompounded": 6.49542954484994,
                                "oneMonth": -0.7045829288904182,
                                "oneMonthCompounded": -8.1349212140174,
                                "oneWeek": -0.22504280681068245,
                                "oneWeekCompounded": -10.940608937192165
                            },
                            {
                                "date": "2023-09-04T00:00:00.000Z",
                                "price": 2.215162,
                                "yearToDate": 4.283355051558813,
                                "yearToDateCompounded": 6.330067678599849,
                                "twelveMonths": 7.190946100204343,
                                "twelveMonthsCompounded": 7.190946100204343,
                                "oneMonth": -0.6388254439303243,
                                "oneMonthCompounded": -7.402214552501352,
                                "oneWeek": -0.21406234655938494,
                                "oneWeekCompounded": -10.435146595882038
                            },
                            {
                                "date": "2023-09-01T00:00:00.000Z",
                                "price": 2.218219,
                                "yearToDate": 4.427269680101853,
                                "yearToDateCompounded": 6.6282984446884186,
                                "twelveMonths": 7.051166660473296,
                                "twelveMonthsCompounded": 7.051166660473296,
                                "oneMonth": -0.9093284790547607,
                                "oneMonthCompounded": -10.38241039644674,
                                "oneWeek": 0.060084253279502065,
                                "oneWeekCompounded": 3.13732775772515
                            }
                        ],
                        "metadata": {
                            "statuscode": 200
                        }
                    };        
                    
                    return mockObj;
                    break;
     }
}


module.exports = {delegateMockDataPerService};