import * as inputs from '../../resources/extra/utils.js';

//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/api/v1/instruments/stocks/mappings#`,
    2:`/api/v1/instruments/historical/financial/ratios#?startYear=2023&endYear=2023&periodType=ANUAL&quarter=1&instrumentId=1%2F12609%2F0%2FAAPL*`,
    3:`/api/v3/instruments/report/market/capitalization#?instrumentKey=1%2F12576%2F0%2FAC*`,
    4:`/api/v1/healthcheck#`,
    5:`/api/v3/instruments/stock/screener#?typeKey=SIC_ETF`,
    6:`/api/v3/instruments/columns#?exchangeId=0&marketTypeId=1&valueTypeId=13088`,
    7:`/api/v1/instruments/historical/financial/ratios#?startYear=2019&endYear=2022&periodType=ANUAL&quarter=1&instrumentId=1/12609/0/AAPL*`,
    8:`/api/v1/instruments/stocks/recommendation-trends#?instrumentKey=1%2F12576%2F0%2FQ*`,
    9:`/api/v1/instruments/report/market/capitalization#?instrumentId=1%2F12609%2F0%2FAAPL*`,
    10:`/api/v3/instruments/detail/participation#?instrumentKey=4/12576/2/AC*&includeTotals=true`,
    11:`/api/v3/instruments/detail/intraday#?offset=1&limit=1&page=1&rows=1&instrumentKey=1/12576/0/AC*`,
    12:`/api/v3/instruments/historical/chart#?instrumentKey=1/12576/0/AC*&period=1&interval=D&useXigniteAdapter=false`,
    13:`/api/v3/instruments/intraday/chart#?instrumentKey=1/12576/0/AC*&period=1&interval=1&useXigniteAdapter=false`,
    14:`/api/v3/instruments/detail/intraday/report#?instrumentKey=1/12576/0/AC*&startDate=2023-09-10&endDate=2023-09-11&periodicity=0&buyerId=0&sellerId=0&page=1&rows=1`,
    15:`/api/v3/instruments/detail/depth#?instrumentKey=2/30/1/AC*.BI`,
    16:`/api/v3/instruments/detail/xbrlReport#?instrumentKey=1/12576/0/AC*&exercise=2021&period=0`,
    17:`/api/v3/instruments/historical/financialRatios/multiples#?instrumentKey=1/12576/0/AC*&startDate=2023-09-11&endDate=2023-09-15`,
    18:`/api/v3/report/brokerageFirms/transactionsExtract#`,
    19:`/api/v1/instruments/stocks/estimates#?instrumentKey=1/12576/0/AC*&startYear=2022&endYear=2023&periodType=Anual&quarter=0`,
    20:`/api/v3/instruments/last/dynamic#?marketTypeId=1&condition=bottom&value=porcentaje&limit=1&fields=descripcion,fechaPrecioActual,hora,bolsa,oportunidadInformacion,porcentaje,precioActual,precioAnterior,tipomercado,tipovalor,variacion,volumenHecho,volumenTotal,instrumentKey&valueTypeIds=12576,12609,12610,12611,12613,13088,13361,17222,12320`,
    21:`/api/v3/instruments/detail/financial/annual#?instrumentKey=1/12576/0/AC*&exercise=2017,2018,2019,2020,2021`,
    22:`/api/v3/instruments/report/sectorial/analysis/multiples#?instrumentKey=1/12576/0/AC*&offset=0&limit=2&order=DESC`,
    23:`/api/v1/instruments/report/local/recommendations#?instrumentId=1%2F12576%2F0%2FAC*`,
    24:`/api/v3/instruments/report/brokerageFirms#?startDate=2022-09-14&endDate=2023-09-14&instrumentKey=1%2F12576%2F0%2FAC*`,
    25:`/api/v1/instruments/stocks/price-target#?instrumentKey=1/12576/0/AC*&targetCurrency=MXN`,
    26:`/api/v3/instruments/last#?instrumentKey=${inputs.getRandomBMVInstrumentKey()}`,
    27:`/api/v3/instruments/detail/profile#?instrumentKey=1/12576/0/AC*`,
    28:`/api/v3/instruments/historical/fund/returns#?instrumentKey=1/13618/0/APIBONOM5&startDate=2023-09-01&endDate=2023-09-05`, 
}
const objPendient = {
   
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}
module.exports={obj,getIndexNumber}
