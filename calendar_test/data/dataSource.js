import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/calendar/api/v1/economic-calendar?startDate=2023-10-04&endDate=2023-10-05&limit=1`,
    2:`/calendar/api/v1/holidays#?startDate=2022-01-01&endDate=2022-10-15`,

    //busines-days
    3:`/calendar/api/v1/business-days#?startDate=2022-02-15&endDate=2022-02-16`,
    4:`/calendar/api/v1/business-days/can-trade#`,
    //coupons-payments
    5:`/calendar/api/v1/coupons-payments#?limit=1&instrumentKey=32780/320/2002/91%20CICB%2008-2`,
    //redemption-payment
    6:`/calendar/api/v1/redemption-payment#?startDate=2023-01-02&endDate=2023-05-31&order=ASC&offset=0&limit=1`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}