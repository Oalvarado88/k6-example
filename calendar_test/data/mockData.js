let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/calendar/api/v1/economic-calendar':
        mockObj=[
            {
                "id": 5178,
                "date": "2023-10-05",
                "title": "Encuesta quincenal de expectativas de analistas de Citibanamex",
                "description": "Es una encuesta de expectativas de las principales variables macroeconómicas del país, como inflación, crecimiento, evolución del tipo de cambio y de las tasas de interés, levantada por el banco Citibanamex.",
                "currentValue": null,
                "previousValue": null,
                "units": null,
                "country": {
                    "id": 1,
                    "name": "México"
                }
            }
        ];        
        return mockObj;
        break;
    case '/calendar/api/v1/holidays':
        mockObj={
            "data": [
                {
                    "instrumento": "DJUSPM",
                    "descripcionInstrumento": "DOW JONES U.S. GOLD MINING INDEX",
                    "precioActual": 91.169998,
                    "variacion": 1.610001,
                    "porcentaje": 1.8,
                    "precioMaximoDia": 91.220001,
                    "precioMinimoDia": 89.57,
                    "hora": "16:20",
                    "fechaPrecioActual": "10-10-2023",
                    "precioAnterior": 89.559998,
                    "fechaPrecioAnterior": "09-10-2023",
                    "precioApertura": 90.220001,
                    "volumenAcumulado": "9823986",
                    "volumenTotal": "9823986",
                    "rendimientoSemanal": 7.893488757396458,
                    "rendimientoMensual": -1.1171355556772802,
                    "rendimientoAnual": -1.9466608857662655,
                    "rendimientoAlAnio": -16.480400383454636,
                    "rendimientoAlMes": 3.1918501127138432,
                    "oportunidadInformacion": "realTime",
                    "descripcion": "DJUSPM",
                    "uniqueKey": "2048/50/7/DJUSPM"
                },
                {
                    "instrumento": "DJUSST",
                    "descripcionInstrumento": "DOW JONES U.S. STEEL INDEX",
                    "precioActual": 550.869995,
                    "variacion": 2.51001,
                    "porcentaje": 0.46,
                    "precioMaximoDia": 559.5,
                    "precioMinimoDia": 550.049988,
                    "hora": "16:20",
                    "fechaPrecioActual": "10-10-2023",
                    "precioAnterior": 548.359985,
                    "fechaPrecioAnterior": "09-10-2023",
                    "precioApertura": 550.049988,
                    "volumenAcumulado": "13558946",
                    "volumenTotal": "13558946",
                    "rendimientoSemanal": 1.6834323950161545,
                    "rendimientoMensual": -0.897706343874795,
                    "rendimientoAnual": 34.542299656915766,
                    "rendimientoAlAnio": 16.41869976050557,
                    "rendimientoAlMes": 0.527394181221339,
                    "oportunidadInformacion": "realTime",
                    "descripcion": "DJUSST",
                    "uniqueKey": "2048/50/7/DJUSST"
                }
            ],
            "metadata": {
                "statuscode": 200
            }
        };        
        return mockObj;
        break;
    case '/calendar/api/v1/business-days':
        mockObj=[
            {
                "date": "2023-12-28",
                "open": "07:50:00",
                "close": "15:10:00"
            },
            {
                "date": "2023-12-29",
                "open": "07:50:00",
                "close": "15:10:00"
            }
        ];
        return mockObj;
        break;

    case '/calendar/api/v1/business-days/can-trade':
            mockObj={
                "can-trade": true
              };
            return mockObj;
            break;
    case '/calendar/api/v1/coupons-payments':
        mockObj={
            "coupons": [
                {
                    "valueType": "91",
                    "issuer": "CICB",
                    "serie": "08-2",
                    "date": "2023-10-10T00:00:00.000Z",
                    "faceValue": 100,
                    "couponRate": 15.7102,
                    "firstCouponDate": "2023-09-25T00:00:00.000Z",
                    "lastCouponDate": "2023-10-25T00:00:00.000Z",
                    "couponNumber": 186,
                    "row": "1"
                }
            ],
            "totalRows": 32
        };
        return mockObj;
        break;
    case '/calendar/api/v1/redemption-payment':
        mockObj={
            "redemptions": [
                {
                    "valueType": "2U",
                    "issuer": "CBIC002",
                    "serie": "300117",
                    "redemptionDate": "2023-01-02T00:00:00.000Z",
                    "faceValue": 100,
                    "redemptionRate": "0",
                    "row": "1"
                }
            ],
            "totalRows": 307958
        };
        return mockObj;
        break;
     }
}


module.exports = {delegateMockDataPerService};