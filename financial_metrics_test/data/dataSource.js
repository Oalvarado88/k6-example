import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    //valuation-ratios
    1:`/financial-metrics/api/v1/valuation-ratios/valuation-daily#?issuer=CLNX&isin=ES0105066007&limit=1&offset=0&orderBy=asOfDate&order=DESC&mock=false`,
    2:`/financial-metrics/api/v1/valuation-ratios/valuation-5y-end#?issuer=CLNX&startDate=2022-01-01&endDate=2023-11-16&limit=1`,
    3:`/financial-metrics/api/v1/valuation-ratios/valuation-10y-end#?startDate=2022-01-01&endDate=2023-11-16&limit=1&offset=0&orderBy=asOfDate&order=DESC&mock=false&issuer=CLNX`,
    4:`/financial-metrics/api/v1/valuation-ratios/valuation-monthly#?issuer=CLNX&startDate=2022-01-01&endDate=2023-11-16&limit=1&offset=0&orderBy=asOfDate&order=DESC&mock=false`,
    ///financial-health-ratios
    5:`/financial-metrics/api/v1/growth-ratios/growth-ratios-quarterly#?issuer=CLNX&isin=NL0010273215&startDate=2013-01-01&endDate=2024-07-04&limit=1&offset=0&orderBy=reportDate&order=ASC&mock=false`,
    6:`/financial-metrics/api/v1/growth-ratios/growth-ratios-annual#?issuer=ASML&isin=NL0010273215&startDate=2013-01-01&endDate=2024-07-04&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=fals`,
    //efficiency-ratios
    7:`/financial-metrics/api/v1/efficiency-ratios/efficiency-ratios-quarterly#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2023-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    8:`/financial-metrics/api/v1/efficiency-ratios/efficiency-ratios-annual#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2024-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    9:`/financial-metrics/api/v1/efficiency-ratios/efficiency-ratios-ttm#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2024-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    //profitability-ratios
    10:`/financial-metrics/api/v1/profitability-ratios/profitability-ratios-quarterly#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2024-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    11:`/financial-metrics/api/v1/profitability-ratios/profitability-ratios-annual#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2024-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    12:`/financial-metrics/api/v1/profitability-ratios/profitability-ratios-ttm#?issuer=AAAU%2A&isin=AAAU%2A&startDate=2022-01-01&endDate=2024-11-16&limit=1&offset=0&orderBy=reportDate&order=DESC&mock=false`,
    //Equities
    13:`/financial-metrics/api/v1/equities/catalogue#?issuers=AMXB%2CARA%2CASURB%2CAUTLANB%2CAXTELCPO%2CAZTECACPO%2CBACHOCOB&isins=MX01AM050019%2CMXP001161019%2CMXP001661018%2CMXP0598H1110%2CMX01AX040009%2CMX01AZ060013%2CMX01BA1D0003&limit=1&offset=0&orderBy=issuer&order=DESC&mock=false`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}