import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    1:`/derivatives/api/v1/catalogue#?exchangesIds=f55e51d8-5195-4c70-9e3c-c71abcf80b66,df109bcb-edc5-4aa8-b91c-346cc2c6fb5a,299d1fdb-9c19-4d49-9e23-3f63fed3c580,"df109bcb-edc5-4aa8-b91c-346cc2c6fb5a,7be04c79-01fc-4f9e-bcff-4783f5d9e041&contractsIds=5ece9c5b-f950-4797-b3fe-0fc6970137b0,d8a0fa05-77da-400c-84e1-64e7d3051370,564cf9b5-a652-4372-9a0e-6703eea30c55,76fde714-77c7-4883-a6d6-e6ae876e921a&limit=1&offset=0&orderBy=symbol&order=ASC&mock=false`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}