let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/derivatives/api/v1/catalogue':
        mockObj={
            "derivatives": [
                {
                    "id": "4cc2c1b4-bcf9-4655-a2fd-f9a7d45d0e69",
                    "marketTypeId": 1024,
                    "valueTypeId": 207,
                    "exchangeTypeId": 200,
                    "instrumentKey": "1024/207/200/100X1SWAP",
                    "symbol": "100X1SWAP",
                    "underlyingAssetName": "100X1",
                    "exchange": "MEXDER",
                    "mexderContractId": 7759,
                    "venueDate": "2031-01-31T00:00:00.000Z",
                    "description": "SWAP TASA TIIE",
                    "type": "SWAP"
                }
            ],
            "countRows": 1,
            "totalRows": 422
        };        
        return mockObj;
        break;
}
}

module.exports = {delegateMockDataPerService};