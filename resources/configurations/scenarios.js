 
 function scenario(value,classConsumer){
  var result;
  switch (value) {
    case 5: //RAMP - configuración para pruebas de rampa
      return result = {
        stages: [
          { duration: '5m', target: 60 }, // simulate ramp-up of traffic from 1 to 60 users over 5 minutes.
          { duration: '10m', target: 60 }, // stay at 60 users for 10 minutes
          { duration: '3m', target: 100 }, // ramp-up to 100 users over 3 minutes (peak hour starts)
          { duration: '2m', target: 100 }, // stay at 100 users for short amount of time (peak hour)
          { duration: '3m', target: 60 }, // ramp-down to 60 users over 3 minutes (peak hour ends)
          { duration: '10m', target: 60 }, // continue at 60 for additional 10 minutes
          { duration: '5m', target: 0 }, // ramp-down to 0 users
        ],
      }
      break;
    case 4://LOAD - configuración para pruebas de carga
    return result ={
      stages: [
        { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
        { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
        { duration: '5m', target: 0 }, // ramp-down to 0 users
      ],
    }
    break;
    case 3: //SOAK - Pruebas de confiabilidad
      return result ={
        stages: [
          { duration: '2m', target: 400 }, // ramp up to 400 users
          { duration: '3h56m', target: 400 }, // stay at 400 for ~4 hours
          { duration: '2m', target: 0 }, // scale down. (optional)
        ],
      }
      break;
    case 2://STRESS - configuración para pruebas de estrés
      return result ={
        
        stages: [
          { duration: '2m', target: 100 }, // below normal load
          { duration: '5m', target: 100 },
          { duration: '2m', target: 200 }, // normal load
          { duration: '5m', target: 200 },
          { duration: '2m', target: 300 }, // around the breaking point
          { duration: '5m', target: 300 },
          { duration: '2m', target: 400 }, // beyond the breaking point
          { duration: '5m', target: 400 },
          { duration: '10m', target: 0 }, // scale down. Recovery stage.
        ],
        
      }
      break;
    case 1://TEST - Prueba con 1 hit los servicios por API (smokeTest)
      return result = {
        scenarios: {
          
          SmokeTest: {
            executor: 'per-vu-iterations',
            gracefulStop: '10m',
            vus: 1,
            iterations: 1,
            maxDuration: '10m',
            exec: classConsumer,
          },
          
        },
      }
        break;
        case 6:
          return result = {
            stages: [
              { duration: '1m', target: 1 }, // below normal load
              {exec:classConsumer}
            ],
          }
        default:
  }
 }

module.exports={scenario }