import * as auth_data from './authentication.js'
var http = require('k6/http')
import { check, sleep } from 'k6'
import {Rate} from 'k6/metrics'
import jsonCompare from '../extra/jsonCompare.js'
export let errorRate = new Rate('errors')

let response  = null;

function login(url,payload,headers) {
    
    response = http.post(url,payload,headers);
    const jsonString =response.body
    const jsonObject = JSON.parse(jsonString);
    const accessToken = jsonObject.access_token;
    
    auth_data.headers["Authorization"] = accessToken;

    sleep(1);
    check(response,{
        '[POST] Login status was 200': r=>r.status ==200,
    // 'body size is 11,105 bytes': (r) => r.body.length == 11105,
    // 'verify homepage text': (r) =>
    // r.body.includes('Collection of simple web-pages suitable for load testing'),
    }) 
    return response;
}
function post (url,payload,headers) {
    response = http.post(url,payload,headers);
    sleep(1);
    return response;
}
function get(url,path,params,headers) {
    response = http.get(url+path+params,headers, {tags:{my_tag:"securities Prices"}});
    sleep(5);
    
    check(response,{
        '[GET] Requests success procesed [SC 200]' : r=>r.status ==200
       }) 

    if(response.status !=200 && response.status!=0){
        
        console.error("FAILED WITH STATUS CODE= " +response.status + " ---> " +path+params)
        console.info("RESPONSE:__"+response.body)
    }

    return response;
}

function postGraphQL (url,query,headers,mockStructure) {
    response = http.post(url,JSON.stringify({ query: query }),headers);
    sleep(1);

    check(response,{
        '[POST] GraphQL status was 200': r=>r.status ==200
    }) 
    if(response.status !=200 && response.status!=0){
        
        console.error("FAILED WITH STATUS CODE= " +response.status)
    }else{
        jsonCompare.compareDataFunction(mockStructure,response.body)
            check(jsonCompare,{
            'Assert ->Json Fields comparation' : r => r.flagFieldsCompare == true,
            'Assert ->Json Key comparation' : r => r.flagKeysCompare ==true
            })
    }
    return response;
}
function getWithMockValidation(url,path,params,headers,mockStructure){
    response = http.get(url+path+params,headers);
    sleep(1);
    check(response,{
        '[GET] Requests success procesed [SC 200]' : r=>r.status ==200
       }) 

    if(response.status !=200 && response.status!=0){
        
        console.error("FAILED WITH STATUS CODE= " +response.status + " ---> " +path+params)
    }else{
        jsonCompare.compareDataFunction(mockStructure,response.body)
            check(jsonCompare,{
            'Assert ->Json Fields comparation' : r => r.flagFieldsCompare == true,
            'Assert ->Json Key comparation' : r => r.flagKeysCompare ==true
            })
    }
    

    return response;
}

function getProcedureWithStaticMultipleValidations(url,path,params,headers,messageToValidate){
    response = http.get(url+path+params,headers);
    sleep(2);
    ////Sacar el numero de VU y los bits del request
   // console.log(`res´pmse body length ${response.body.length} for VU= ${__VU} ITERA ${__ITER}` ):
    const checks = check(response,{
        '[GET] Requests success procesed [SC 200]' : r=>r.status ==200
         ,'body size isn´t 0 bites :' : (r) => r.body.length == 0
         , 'verify response body: ' : (r) => r.body.includes(messageToValidate)
       }) 
       errorRate.add(!checks)
    return response;
}
function getProcedureFailRate(){
    
}


module.exports={
    login, post, get,postGraphQL, getWithMockValidation
}