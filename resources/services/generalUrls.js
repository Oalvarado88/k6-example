var preprodEnv = {
    "hub_workspace_url":"https://hub-workspace-pre2.infosel-digitalfactory.com",
    "hub_market_url":"https://hub-market-pre2.infosel-digitalfactory.com",
    "api_market_url":"https://api-market-preprod.infosel-digitalfactory.com",
    "api_url":"https://api-preprod-test.infosel.com",
    "api_market_url":"https://api-market-preprod.infosel-digitalfactory.com",
    "iam_admin_url":"https://iam-admin-preprod.infosel-digitalfactory.com"
}
var qaEnv = {
    "hub_workspace_url":"https://hub-workspace-qa.infosel-digitalfactory.com",
    "hub_market_url":"https://hub-market-qa.infosel-digitalfactory.com",
    "api_market_url":"https://api-market-qa.infosel-digitalfactory.com",
    "api_url":"https://api-qa.infosel.com",
    "api_market_url":"https://api-market-qa.infosel-digitalfactory.com",
    "iam_admin_url":"https://iam-admin-qa.infosel-digitalfactory.com",
    "api_url_test":"https://api-qa-test.infosel.com",
    "news_url":"https://hub-news-qa.infosel-digitalfactory.com/",
}

module.exports = {
    "hub_workspace_url":"https://hub-workspace-qa.infosel-digitalfactory.com",
    "hub_market_url":"https://hub-market-qa.infosel-digitalfactory.com",
    "api_market_url":"https://api-market-qa.infosel-digitalfactory.com",
    "api_url":"https://api-qa.infosel.com",
    "api_market_url":"https://api-market-qa.infosel-digitalfactory.com",
    "iam_admin_url":"https://iam-admin-qa.infosel-digitalfactory.com",
    "api_url_test":"https://api-qa-test.infosel.com",
    "news_url":"https://hub-news-qa.infosel-digitalfactory.com/",
    preprodEnv,
    qaEnv

}