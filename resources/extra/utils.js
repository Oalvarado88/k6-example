function getRndInteger(limit){
    return Math.floor(Math.random()*limit);
}
function getRandomString(numberChar){
var text=numberChar;

return text;
}
function getRandomDate(daysNumber){
    var dateOutput = daysNumber;
    return dateOutput;
}
function getRandomInstrument(clave){
    //format clave would be 1/1111/1/ 
    return "";
}
function getRanndomIssuer(){
    
    return "DEUDAOP";
}
function getRandomSerie(){
    return "B1";
}
//XIGNITE TEST
function getRandomInstrumentForXigniteTest(){
    var objectResultBmvInstrumentKey={
        
       0:  '2048/50/7/DJI', //INDICE
       1:'4096/50/25/ARSMXN', //DIVISA
       2:'32/101/16/AAON',//INSTRUMENTO  EUU
      
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}

//GET INSTRUMENT AT https://hub-market-qa.infosel-digitalfactory.com/api/v3/instruments?marketTypeIds=2048&valueTypeIds=50&exchangeIds=44
function getRandomIndexInstrumentKey(){
    var objectResultBmvInstrumentKey={
        
       0:  '2048/50/7/DJI',
       1:'2048/50/7/BRIC50D',
       2:'2048/50/7/CH80',
       3:'2048/50/14/COMPX',
       4:'2048/50/7/DJAT',
       5:'2048/50/7/DJC',
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}

function getRandomFuturesInstrumetnKey(){
    var objectResultBmvInstrumentKey={
        
       0:  '1024/201/2/BOF9',
       1:'1024/201/2/BOH8',
       2:'1024/201/2/BOH9',
       3:'1024/201/2/BOK8',
       4:'1024/201/2/BOK9',
       5:'1024/201/2/BON0',
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}

function getRandomStockVal(){
    var objectResultBmvInstrumentKey={
        
       0:  '1/13619/0/MEXCAPA1',
       1:'1/13619/0/MEXCAPA',
       2:'1/13619/0/MEXCAPA2',
       3:'1/13619/0/MEXCAPB1',
       4:'1/13619/0/MEXCAPB2',
       5:'1/13619/0/MEXPLUSA1',
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getRandomDIVISASInstrumentKey(){
    var objectResultBmvInstrumentKey={
        
       0:  '4096/50/25/AEDUS',
       1:'4096/50/25/ALLUS',
       2:'4096/50/25/ARSMXN',
       3:'4096/50/25/ARSUS',
       4:'4096/50/25/AUDGBP',
       5:'4096/50/25/AUDMXN',
       6:'4096/50/25/AUDNOK',
       7:'4096/50/25/AUDSS',
       8:'4096/50/25/AUDUS',
       9:'4096/50/25/AUDVS',
       10:'4096/50/25/AUDYD'
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getRandomEUUInstrumentKey(){
    var objectResultBmvInstrumentKey={
        
       0:'32/2/21/A',
        1:'32/101/16/AAON',
        2:'32/102/1/ACU',
        3:'32/101/16/CSCO'
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getRandomConsolidadoInstrumentKey(){
    var objectResultBmvInstrumentKey={
        
       0:'4/12320/2/CREAL*',
        1:'4/12576/2/AC*',
        2:'4/12609/2/*',
        3:'4/12609/2/0JN9N',
        4:'4/12611/2/CETETRCISHRS',
        5:'4/12613/2/ANB*',
        6:'4/12617/2/',
        7:'4/12626/2/',
        8:'4/13088/2/',
        9:'4/13361/2/',
        10:'4/17222/2/',
        11:'4/17989/2/',
        12:'4/17992/2/'
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getRandomBIVAInstrumentKey(){
    var objectResultBmvInstrumentKey={
       0:'2/10/1/AA1CK17.BI',
        1:'2/20/1/0JN9N.BI',
        2:'2/30/1/AC*.BI*',
        3:'2/50/1/ANB*.BI',
        4:'2/80/1/DANHOS13.BI',
        5:'2/100/1/ANGELD10.BI'
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getRandomBMVInstrumentKey(){
    var objectResultBmvInstrumentKey={
        
       0:'1/12609/0/1109N',
        1:'1/12320/0/UNIFINA',
        2:'1/12320/0/LASEG*',
        3:'1/12320/0/GNP*',
        4:'1/12576/0/AC*',
        5:'1/12576/0/AGRIEXPA',
        6:'1/12576/0/AGUA*',
        7:'1/12609/0/0JN9N',
        8:'1/12609/0/0QBON',
        9:'1/12610/0/ANGELD10',
        10:'1/12611/0/CETETRCISHRS',
        11:'1/12613/0/ANB*',
        12:'1/12617/0/4BRZN',
        13:'1/12626/0/AA1CK17',
        14:'1/13088/0/FINAMEXO',
        15:'1/13361/0/BBAJIOO',
        16:'1/17222/0/AGRO22',
        17:'1/17989/0/FCFE18',
        18:'1/17992/0/FHIPO14'
    }
    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}
function getIndexToExecuteTest(obj,number){
    const limits= {0:1,1:1};
    if(number==0){
        number=Object.keys(obj).length
        limits[0]=1;
        limits[1]=number;
    }else{
        limits[0]=number;
        limits[1]=number;
    }
    return limits;
}
function getRandomPortfolio(){

    var randomIndex = Math.floor(Math.random()*(Object.keys(objectResultBmvInstrumentKey).length))
    return objectResultBmvInstrumentKey[randomIndex];
}

module.exports={getRandomInstrumentForXigniteTest,
    getRandomDIVISASInstrumentKey,getRandomFuturesInstrumetnKey,getRandomIndexInstrumentKey,
    getRandomEUUInstrumentKey,getRandomStockVal,getRandomConsolidadoInstrumentKey,getRandomBIVAInstrumentKey, 
    getRandomDate, getRandomString,getRandomInstrument,getRanndomIssuer,getRandomSerie,getIndexToExecuteTest,getRandomBMVInstrumentKey
}