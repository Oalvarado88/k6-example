

var flagKeysCompare = true
var flagFieldsCompare = true;
var count = 0;
function compareDataFunction(localJson,remoteJson){
    remoteJson = JSON.parse(remoteJson)

            flagKeysCompare = false
            flagFieldsCompare = false;


            //VALIDACIÓN SI ES UN JSON O ES UN ARRAY, TIENEN TRATOS DIFERENTES CADA TIPO DE DATOS
       
                const isEqual = (currentValue) => currentValue == true;

            let matches = []
            const localResponseFields = [];
            for (const field in localJson) {
                localResponseFields.push(field);            }
            const remoteResponseFields = [];
            for (const field in remoteJson) {
                remoteResponseFields.push(field);
            }
            matches.push(compareKeys(localResponseFields, remoteResponseFields))            

            for(const fieldName in localJson){
                matches.push(comparePropertiesType(localJson[`${fieldName}`], remoteJson[`${fieldName}`],`${fieldName}`,fieldName))

                if(matches.every(isEqual)){
                    count++; console.info(count + ' Fields Equals -> Match´s'); 
                } 
                else{ console.error('ERROR FIELD MATCH'); flagKeysCompare=false;flagFieldsCompare=false;}
            }
            
}

const comparePropertiesValues = (propA, propB, property) => {
    if(propA == propB){
        console.info(`${property} => ${propA} = ${propB}`);
        return true
    }
    console.error(`${property} => ${propA} <> ${propB}`);
    
    return false
}
const comparePropertiesType = (propertyA,propertyB, fieldName) =>{
    const propertyAA= typeof propertyA 
    const propertyBB= typeof propertyB
        if( propertyAA ==  propertyBB){
            console.info(`${fieldName} -> ${propertyAA} == ${propertyBB}`);
            return true
        } else{
         
        console.error(`Error en la comparación ${propertyA} tipado incorrecto del esperado: 
        Expected value: <${propertyAA}> Actual Value: <${propertyBB}>`)
        return false;}
}
const compareKeys = (propertyA,propertyB) =>{
    if( JSON.stringify(propertyA) == JSON.stringify(propertyB)){

            console.info(`${propertyA} == ${propertyB}`);
            return true
        } 
         
        console.error(`Keys comparadas <${propertyA}> VS <${propertyB}>`)
        return false;
}

module.exports= {flagKeysCompare,flagFieldsCompare,compareDataFunction}