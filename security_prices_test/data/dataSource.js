import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    //MAIN
    1:`/securities-prices/api/v1/suscription#?source=FX&symbol=ARSMXN`,
    //TRADE
    2:`/securities-prices/api/v1/trade/last-trade#?source=BMV&symbol=CEMEXCPO`,
    3:`/securities-prices/api/v1/trade/historical#?symbol=GOOG&exchange=NYSE&startDate=2023-08-01&endDate=2023-08-01&order=DESC&useXigniteAdapter=true`,
    4:`/securities-prices/api/v1/trade/best-bid-offer#?source=BMV&symbol=AC*`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}