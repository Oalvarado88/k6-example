let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/securities-prices/api/v1/suscription':
        mockObj={
            "marketTypeId": 4096,
            "valueTypeId": 50,
            "exchangeId": 25,
            "symbol": "ARSMXN",
            "uniqueKey": "4096/50/25"
        }
        return mockObj;
        break;
    case '/securities-prices/api/v1/trade/last-trade':
        mockObj = {
    "symbol": "CEMEXCPO",
    "exchange": "BMV",
    "tradeTime": "2023-09-08T13:59:57",
    "tradeId": 7186,
    "executedPrice": 12.7,
    "executedQuantity": 1350,
    "closingPrice": 13.02
};        return mockObj;

        break;

    case '/securities-prices/api/v1/trade/historical':
        mockObj = {
            "historicalTrade": [
                {
                    "datetime": "2023-08-01T00:00:00.000Z",
                    "open": 130.855,
                    "high": 132.92,
                    "low": 130.75,
                    "close": 131.89,
                    "volume": 22219012
                }
            ],
            "countRows": 1,
            "totalRows": 1
        };
        return mockObj;

        break;

    case '/securities-prices/api/v1/trade/best-bid-offer':
        mockObj = {
            "symbol": "AC*",
            "timestamp": "2023-09-08T14:59:59.498036",
            "spread": 0.86,
            "sellVolume": 30600,
            "buyPrice": 160.43,
            "date": "2023-09-08",
            "sellPrice": 161.29,
            "hour": "13:59",
            "percentageSpread": 0.01,
            "buyVolume": 11800
        };
        return mockObj;

        break;
     }
}


module.exports = {delegateMockDataPerService};