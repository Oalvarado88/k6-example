# README #

**Pre requisitos:**

* K6 Instalado: https://k6.io/docs/get-started/installation/

* NodeJS Instalado: https://nodejs.org/en/download/

* Instalar Go: https://go.dev/doc/install

**Pasos de instalación:**

* Correr el comando “npm install" dentro de la carpeta del repositorio

* Correr el comando "go install go.k6.io/xk6/cmd/xk6@latest" (solo corre si se tiene instalado el "go" mencionado en los pre requisitos)

* Correr comando de ejecución “npm run funds-smoke" para validar que todo valla bien 

___
**Comandos disponibles:**
    
    "install-xk6":"go install go.k6.io/xk6/cmd/xk6@latest"
    "win-build":"xk6 build --with github.com/grafana/xk6-output-prometheus-remote@latest",
    "linux-Build":"xk6 build --with github.com/grafana/xk6-output-prometheus-remote@latest"
     "xk6 build --with github.com/grafana/xk6-output-prometheus-remote@latest"
    "hub-market-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=1&&set ENV=1 && k6.exe run ./market_tests/index.js -o xk6-prometheus-rw",
    "hub-market-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2 && k6.exe run ./market_tests/index.js -o xk6-prometheus-rw",
    "hub-market-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3 && k6.exe run ./market_tests/index.js -o xk6-prometheus-rw",
    "hub-market-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4 && k6.exe run ./market_tests/index.js -o xk6-prometheus-rw",
    "hub-market-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5 && k6.exe run ./market_tests/index.js -o xk6-prometheus-rw",

    "funds-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=1&& k6.exe run ./funds_test/index.js -o xk6-prometheus-rw",
    "funds-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2&& k6.exe run ./funds_test/index.js -o xk6-prometheus-rw",
    "funds-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3&& k6.exe run ./funds_test/index.js -o xk6-prometheus-rw",
    "funds-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4&& k6.exe run ./funds_test/index.js -o xk6-prometheus-rw",
    "funds-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5&& k6.exe run ./funds_test/index.js -o xk6-prometheus-rw",

    "hub-workspace-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=1 && k6.exe run ./workspace_test/index.js -o xk6-prometheus-rw",
    "hub-workspace-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2 && k6.exe run ./workspace_test/index.js -o xk6-prometheus-rw",
    "hub-workspace-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3 && k6.exe run ./workspace_test/index.js -o xk6-prometheus-rw",
    "hub-workspace-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4 && k6.exe run ./workspace_test/index.js -o xk6-prometheus-rw",
    "hub-workspace-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5 && k6.exe run ./workspace_test/index.js -o xk6-prometheus-rw",
    
    "widgets-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=1 && k6.exe run ./nestjs_test/index.js",
    "widgets-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2 && k6.exe run ./nestjs_test/index.js",
    "widgets-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3 && k6.exe run ./nestjs_test/index.js",
    "widgets-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4 && k6.exe run ./nestjs_test/index.js",
    "widgets-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5 && k6.exe run ./nestjs_test/index.js",

    "security-prices-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://35.245.146.255:32462/api/v1/write&&set TEST_TYPE=1 && k6.exe run ./security_prices_test/index.js -o xk6-prometheus-rw",
    "security-prices-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2 && k6.exe run ./security_prices_test/index.js -o xk6-prometheus-rw",
    "security-prices-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3 && k6.exe run ./security_prices_test/index.js -o xk6-prometheus-rw",
    "security-prices-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4 && k6.exe run ./security_prices_test/index.js -o xk6-prometheus-rw",
    "security-prices-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5 && k6.exe run ./security_prices_test/index.js -o xk6-prometheus-rw",
    
    "etfs-smoke": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=1 && k6.exe run ./etfs_test/index.js -o xk6-prometheus-rw",
    "etfs-stress": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=2 && k6.exe run ./etfs_test/index.js -o xk6-prometheus-rw",
    "etfs-soak": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=3 && k6.exe run ./etfs_test/index.js -o xk6-prometheus-rw",
    "etfs-load": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=4 && k6.exe run ./etfs_test/index.js -o xk6-prometheus-rw",
    "etfs-ramp": "set K6_PROMETHEUS_RW_SERVER_URL=http://34.150.190.88:32462/api/v1/write&&set TEST_TYPE=5 && k6.exe run ./etfs_test/index.js -o xk6-prometheus-rw"
  }