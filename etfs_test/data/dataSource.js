import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = { 
    //CATALOGUE
    1:`/etfs/api/v1/catalogue#?assetClassId=5&limit=1`,
    //METRICS
    2:`/etfs/api/v1/metrics#?instrumentKeys=32/101/16/ENZL`,
    //INTRADAY
    3:`/etfs/api/v1/prices/intraday#?instrumentKey=1/12617/0/GUSH*&date=2023-10-10&interval=1`,
   // 4:`/etfs/api/v1/prices/sync#`, ->POST
    //HISTORICAL
    4:`/etfs/api/v1/prices/historical#?instrumentKey=1/12617/0/GUSH*&startDate=2023-01-01&endDate=2023-07-30&interval=M&limit=1`,
    //:`/etfs/api/v1/prices/historical/sync#`, ->POST
    //PRICES
    5:`/etfs/api/v1/prices#?instrumentKeys=32/2/21/ACP`,
    //:`/etfs/api/v1/prices/prices/sync#`, ->POST
    //OVERVIEW
    6:`/etfs/api/v1/overview#?instrumentKeys=32/2/21/IVW`,
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}