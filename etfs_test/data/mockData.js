
let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/etfs/api/v1/catalogue':
        mockObj={
            "etfs": [
                {
                    "id": 2393,
                    "marketTypeId": 1,
                    "valueTypeId": 12617,
                    "exchangeId": 0,
                    "ticker": "4BRZN",
                    "symbol": "4BRZN",
                    "instrumentKey": "1/12617/0/4BRZN",
                    "name": "ISHARES MSCI BRAZIL UCITS ETF DE ACC",
                    "cusip": null,
                    "isin": "DE000A0Q4R85",
                    "assetClass": null,
                    "subAssetClass": null,
                    "issuer": "4BRZ",
                    "series": "N",
                    "exchange": {
                        "name": "BMV"
                    }
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
    case '/etfs/api/v1/metrics':
        mockObj = {
            "metrics": [
                {
                    "id": "001e8d69-45be-48c3-ad43-e2ebc9cefe33",
                    "instrumentKey": "32/101/16/ENZL",
                    "priceBooksRatio": 1.66,
                    "priceEarningsRatio": 18.74,
                    "premiumDiscount": 0.29,
                    "expenseRatio": 0.5,
                    "epsGrowthRate": null,
                    "medianBidAskSpread": 0.43,
                    "issuer": "FLRN",
                    "updatedAt": "2023-10-10T12:34:58.594Z",
                    "createdAt": "2023-03-03T15:23:44.786Z"
                }
            ],
            "totalRows": 1
        };
        return mockObj;
        break
    case '/etfs/api/v1/prices/intraday':
        mockObj={
            "intradayPrices": [
                {
                    "datetime": "2023-10-10 08:17:00",
                    "open": 683.0000610351562,
                    "close": 683.0000610351562,
                    "high": 683,
                    "low": 683,
                    "volume": 10
                },
                {
                    "datetime": "2023-10-10 08:21:00",
                    "open": 679.0000610351562,
                    "close": 679.0000610351562,
                    "high": 679,
                    "low": 679,
                    "volume": 35
                },
                {
                    "datetime": "2023-10-10 08:30:00",
                    "open": 674.0000610351562,
                    "close": 674.0000610351562,
                    "high": 674,
                    "low": 674,
                    "volume": 139
                },
                {
                    "datetime": "2023-10-10 08:35:00",
                    "open": 674.5000610351562,
                    "close": 674.5000610351562,
                    "high": 674.5,
                    "low": 674.5,
                    "volume": 42
                },
                {
                    "datetime": "2023-10-10 09:04:00",
                    "open": 682.0000610351562,
                    "close": 682.0000610351562,
                    "high": 682,
                    "low": 682,
                    "volume": 10
                },
                {
                    "datetime": "2023-10-10 09:24:00",
                    "open": 684.9700317382812,
                    "close": 684.9900512695312,
                    "high": 684.99,
                    "low": 684.97,
                    "volume": 40
                },
                {
                    "datetime": "2023-10-10 09:46:00",
                    "open": 685.0000610351562,
                    "close": 685.0000610351562,
                    "high": 685,
                    "low": 685,
                    "volume": 330
                },
                {
                    "datetime": "2023-10-10 09:47:00",
                    "open": 685.0000610351562,
                    "close": 685.0000610351562,
                    "high": 685,
                    "low": 685,
                    "volume": 200
                },
                {
                    "datetime": "2023-10-10 09:48:00",
                    "open": 684.0000610351562,
                    "close": 684.0000610351562,
                    "high": 684,
                    "low": 684,
                    "volume": 350
                },
                {
                    "datetime": "2023-10-10 09:49:00",
                    "open": 684.5000610351562,
                    "close": 684.5000610351562,
                    "high": 684.5,
                    "low": 684.5,
                    "volume": 300
                },
                {
                    "datetime": "2023-10-10 09:50:00",
                    "open": 684.5000610351562,
                    "close": 684.5000610351562,
                    "high": 684.5,
                    "low": 684.5,
                    "volume": 450
                },
                {
                    "datetime": "2023-10-10 09:51:00",
                    "open": 684.0000610351562,
                    "close": 684.5000610351562,
                    "high": 684.5,
                    "low": 684,
                    "volume": 500
                },
                {
                    "datetime": "2023-10-10 09:52:00",
                    "open": 684.5000610351562,
                    "close": 684.5000610351562,
                    "high": 684.5,
                    "low": 684.5,
                    "volume": 225
                },
                {
                    "datetime": "2023-10-10 11:46:00",
                    "open": 685.0000610351562,
                    "close": 685.0000610351562,
                    "high": 685,
                    "low": 685,
                    "volume": 15
                },
                {
                    "datetime": "2023-10-10 12:00:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 20
                },
                {
                    "datetime": "2023-10-10 13:06:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 1500
                },
                {
                    "datetime": "2023-10-10 13:15:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 1458
                },
                {
                    "datetime": "2023-10-10 13:16:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 1458
                },
                {
                    "datetime": "2023-10-10 13:17:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 1458
                },
                {
                    "datetime": "2023-10-10 13:18:00",
                    "open": 686.0000610351562,
                    "close": 686.0000610351562,
                    "high": 686,
                    "low": 686,
                    "volume": 1458
                }
            ],
            "totalRows": 20
        };        
        return mockObj;
        break;
    case '/etfs/api/v1/prices/historical':
        mockObj={
            "historicalPrices": [
                {
                    "datetime": "2023-07-28T05:00:00.000Z",
                    "open": 2345,
                    "close": 2400,
                    "high": 2402.53,
                    "low": 2345,
                    "volume": "1641"
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
    case '/etfs/api/v1/prices':
        mockObj={
            "prices": [
                {
                    "id": "b8950d3a-1e33-456e-a8ef-abf56ab1a664",
                    "instrumentKey": "32/2/21/ACP",
                    "price": 6.55,
                    "volume": 134583,
                    "updatedAt": "2023-03-30T14:02:00.000Z",
                    "createdAt": "2022-12-23T14:29:00.000Z"
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
    case '/etfs/api/v1/overview':
        mockObj={
            "overview": [
                {
                    "instrumentKey": "32/2/21/IVW",
                    "fundInception": "2000-05-22T05:00:00.000Z",
                    "benchmarkIndex": "s&p 500(r) growth index",
                    "distributionFrequency": "quarterly",
                    "currency": "usd",
                    "url": "https://www.ishares.com/us/products/239725/ishares-sp-500-growth-etf",
                    "issuer": null
                }
            ],
            "totalRows": 1
        };        
        return mockObj;
        break;
    
     }
     
    
}


module.exports = {delegateMockDataPerService};