import * as auth_data from '../resources/services/authentication.js'
import * as url_data from '../resources/services/generalUrls.js'
import * as middlewaresProcedures from '../resources/services/middlewaresProcedures.js'
import * as dataSource from './data/dataSource.js'
import * as config_data from '../resources/configurations/scenarios.js'
import * as mockData from './data/mockData.js'
//import remote from 'k6/x/remotewrite';

//Configurations available: hub_market_test, stress, soak, load, ramp
let response;
let localJson;
const  config= config_data.scenario(parseInt(__ENV.TEST_TYPE),"news_test")
export const options = config;
var authenticationData = auth_data.qaDataAuth;
var apiUrls = url_data.qaEnv;
var environment = parseInt(__ENV.ENV);
if(environment != 1){
  switch(environment){
    case 2: //PREPRODUCCI�N
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
    case 3: //PRODUCCION
      authenticationData = auth_data.preprodDataAuth; 
      apiUrls = url_data.preprodEnv;
    break;
  }
}
function contentFunctions(){
  // login
  response = middlewaresProcedures.login(`${url_data.iam_admin_url}${auth_data.admin_path}`,
  auth_data.admin_body,auth_data.urlencooded_header);

  const authHeaders = {headers:{Authorization: `Bearer ${auth_data.headers["Authorization"]}`},};
  //Introduce 0 when call this method for execute all endpoints
  //Introduce any number inside object range, for example "5" if do you want execute individual test per only one service
  const limit =dataSource.getIndexNumber(0);
  
  //Test
  let i = limit[1];
  do {
    const fullPath = dataSource.obj[i];
    const path = fullPath.split('#')
    localJson = mockData.delegateMockDataPerService(path[0]);
    console.log(url_data.news_url,path[0],path[1])
    response = middlewaresProcedures.getWithMockValidation(url_data.news_url,path[0],path[1],authHeaders,localJson);
    //console.log(response.body)

    i = i-1;
  } while (i >= limit[0]);
  }
export default function news() {contentFunctions()}
export function news_test() {  contentFunctions()}