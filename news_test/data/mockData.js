let mockObj = null;
function delegateMockDataPerService(path){
switch (path) {
    case '/api/v1/news':
        mockObj={
          "data": [
              {
                  "rowNumber": "1",
                  "newsId": 5927550,
                  "date": "2022-04-01T00:00:00.000Z",
                  "time": "19:26:00",
                  "serviceId": 618,
                  "newsTypeId": 1,
                  "bodyFormat": 0,
                  "fixedServiceCode": null,
                  "header": "Kerry, AMLO ponen sobre la mesa intereses materia energética(1)",
                  "tags": "AMLO,Energía,Reformaeléctrica",
                  "sectors": "Energía",
                  "source": "INFOSEL",
                  "countries": "México",
                  "abstract": "John Kerry, el representante de Estados Unidos para asuntos en materia de cambio climático, viajó esta semana a México para entrevistarse con el presidente de México, Andrés Manuel López Obrador, en la que podría ser considerada una de las reuniones más relevantes para discutir y hacer contrapeso a los cambios en materia energética que pretende materializar el mandatario.",
                  "priority": 1,
                  "numberAttachedFiles": 1,
                  "imageURL": "infosel-financiero.s3.us-east-2.amazonaws.com/ifplus/archivosweb/imagenes/2022/04/01/2022040159275501.jpg"
              }
          ],
          "metadata": {
              "totalRows": 83,
              "page": 1,
              "rows": 1,
              "startRow": 1,
              "finalRow": 1,
              "statuscode": 200
          }
      }
        return mockObj;
        break;
    case '/api/v1/news/services':
        mockObj={
          "data": [
              {
                  "idServicio": 620,
                  "nvcDescripcion": "Actualidad"
              },
              {
                  "idServicio": 604,
                  "nvcDescripcion": "Bolsa"
              },
              {
                  "idServicio": 600,
                  "nvcDescripcion": "Deuda"
              },
              {
                  "idServicio": 608,
                  "nvcDescripcion": "Divisas"
              },
              {
                  "idServicio": 618,
                  "nvcDescripcion": "Economía"
              },
              {
                  "idServicio": 615,
                  "nvcDescripcion": "Empresas"
              },
              {
                  "idServicio": 655,
                  "nvcDescripcion": "Global"
              },
              {
                  "idServicio": 614,
                  "nvcDescripcion": "Mercancías"
              },
              {
                  "idServicio": 663,
                  "nvcDescripcion": "PR Newswire"
              }
          ],
          "metadata": {
              "statuscode": 200
          }
      }
        return mockObj;
        break;
    case '/api/v1/news/services':
        mockObj={
            "data": [
              {
                "serviceId": 1,
                "description": "Mercado"
              }
            ],
            "metadata": {}
          }
        return mockObj;
        break;
    case '/api/v1/news/6231607/detail':
        mockObj={
          "data": [
              {
                  "newsId": 6231607,
                  "date": "2023-09-29",
                  "time": "10:26:00",
                  "priority": 1,
                  "serviceId": 615,
                  "source": "INFOSEL",
                  "header": "Unifor, sindicato Canadá, da a GM hasta 9 oct. para negociar(1)",
                  "abstract": "Unifor, el sindicato que representa a los trabajadores de la industria automotriz en Canadá, fijó el 9 de octubre como la fecha límite para negociar un nuevo acuerdo con General Motors, en momentos en los que la industria enfrenta huelgas de gran calado en Estados Unidos.",
                  "body": "por Patricia Guerrero Medina\n\n\n(Actualiza con detalles a lo largo de la historia)\r\n\r\nInfosel, septiembre. 29.- Unifor, el sindicato que representa a los trabajadores de la industria automotriz en Canadá, fijó el 9 de octubre como la fecha límite para negociar un nuevo acuerdo con General Motors, en momentos en los que la industria enfrenta huelgas de gran calado en Estados Unidos.\r\n\r\nLas negociaciones con la armadora estadounidense buscan establecer un contrato modelo que contempla aumentos significativos en pensiones, salarios, beneficios de salud, entre otros, dijo Unifor en un comunicado.\r\n\r\n\"Hay mucho trabajo por hacer y tenemos confianza en que nuestro equipo negociador estará a la altura del desafío\", dijo el sindicato en el comunicado firmado por Lana Payne, presidenta nacional de Unifor.\r\n\r\nEntre los beneficios que busca negociar el sindicato, que representa a más de cuatro mil 300 miembros, están el mejor paquete salarial jamás negociado en la historia de Unifor; bonos de productividad y calidad de 10 mil dólares; entre otros.\r\n\r\nUnifor fijó el plazo a GM días después de haber alcanzado un acuerdo tentativo de tres años con Ford, con demandas claves que los grandes fabricantes de autos no han aceptado en las negociaciones que mantiene con United Auto Workers (UAW) en Estados Unidos, de acuerdo con algunos medios.\r\n\r\nUAW inició, a mediados de septiembre, la mayor huelga en la historia del sector al involucrar a las tres principales compañías automotrices del país --General Motors, Ford y Stellantis-- en un intento por mejorar las condiciones de sus 150 mil trabajadores afiliados. Hasta este viernes, hay 18 mil trabajadores sindicalizados en paro en las tres compañías.\r\n\r\n\r\n\r\nCorreo electrónico: patricia.guerrero@infosel.com",
                  "tags": "Automotriz,GM,Negociaciones,Sindicato,Unifor",
                  "containsTables": false,
                  "numberAttachedFiles": 1,
                  "symbols": ""
              }
          ],
          "metadata": {
              "statuscode": 200
          }
      }
        return mockObj;
        break;
    case '/api/v1/news/6231607/files':
        mockObj = {
          "data": [
              {
                  "newsId": 6231607,
                  "urlFile": "infosel-financiero.s3.us-east-2.amazonaws.com/ifplus/archivosweb/imagenes/2023/09/29/2023092962316071.jpg",
                  "typeFile": "Imagen",
                  "fileSize": "3097152",
                  "formatFile": "jpg",
                  "relatedText": "Las negociaciones con la armadora estadounidense buscan establecer un contrato modelo que contempla aumentos significativos en pensiones, salarios, beneficios de salud, entre otros, dijo Unifor en un comunicado",
                  "order": 1
              }
          ],
          "metadata": {
              "statuscode": 200
          }
      }
        return mockObj
        break;
    case '/api/v1/news/sectors':
        mockObj = {
          "data": [
              {
                  "idSector": 10,
                  "cveSector": "Energía"
              },
              {
                  "idSector": 15,
                  "cveSector": "Materiales"
              },
              {
                  "idSector": 20,
                  "cveSector": "Industrial"
              },
              {
                  "idSector": 25,
                  "cveSector": "Consumo Discrecional"
              },
              {
                  "idSector": 30,
                  "cveSector": "Productos de Primera Necesidad"
              },
              {
                  "idSector": 35,
                  "cveSector": "Salud"
              },
              {
                  "idSector": 40,
                  "cveSector": "Finanzas"
              },
              {
                  "idSector": 45,
                  "cveSector": "Tecnologías de la Información"
              },
              {
                  "idSector": 50,
                  "cveSector": "Servicios de comunicaciones"
              },
              {
                  "idSector": 55,
                  "cveSector": "Servicios básicos"
              },
              {
                  "idSector": 60,
                  "cveSector": "Bienes raíces"
              }
          ],
          "metadata": {
              "statuscode": 200
          }
      }
        return mockObj
        break;

     }
}


module.exports = {delegateMockDataPerService};