import * as inputs from '../../resources/extra/utils.js';
//The format for new test or services is "Path_text"+"#"+"params"
const obj = {
    //MAIN
    1:`/api/v1/news#?services=600%2C604%2C608%2C614%2C615%2C618%2C620%2C655&words=AMX%2CALFA%2CCEMEX&filter=0&tag=Deuda&period=3&listType=6&rows=1&page=1&includeBreaking=true&excludeTables=false&ambit=interno&onlyTop=false&symbol=ALFAA%2C%20CEMECPO%2C%20AEROMEX%2A&startDate=2022-04-01&endDate=2022-04-01`,
    2:`/api/v1/news/services#`,
    3:`/api/v1/news/6231607/detail#`,
    4:`/api/v1/news/6231607/files#`,
    5:`/api/v1/news/sectors#`
}
function getIndexNumber(number){
    const output = inputs.getIndexToExecuteTest(obj,number)
    return output;
}

module.exports={getIndexNumber,obj}